//
//  ChatHoleDetector.swift
//  togezzer-ios
//
//  Created by Семен Власов on 10.07.2018.
//  Copyright © 2018 tgz. All rights reserved.
//

import RealmSwift

class ChatHoleDetector {
    
    struct Hole {
        
        enum State {
            case empty, loading, failed
        }
        
        let range: CountableClosedRange<Int>
        let state: State
    }
    
    //MARK: - Properties
    private(set) var holes = [Hole]()
    
    private let pageSize = 100
    
    private var chatId: Int
    private var storageService: IStorage
    private var dbStorage: DBStorage

    private var lastSerial = 0
    private var notificationToken: DBNotificationToken?
    private var serials = [Int]()
    
    //Костыль для бага на сервере, где иногда в serial бывают пропуски
    private(set) var loadedEmptySerials: Set<Int> { didSet {
        var dict = storageService.load(key: .emptyMessageSerials) ?? [Int: Set<Int>]()
        dict[chatId] = loadedEmptySerials
        storageService.save(dict, forKey: .emptyMessageSerials)
    }}
    
    //MARK: - Lifecycle
    init(chatId: Int, storageService: IStorage, dbStorage: DBStorage) {
        self.chatId = chatId
        self.dbStorage = dbStorage
        self.storageService = storageService
        self.loadedEmptySerials = storageService
            .load(key: .emptyMessageSerials)?[chatId] ?? Set<Int>()
        subscribeForUpdates()
    }
    
    deinit {
        unsubscribe()
    }
    
    //MARK: - Interface
    func loadIfNeeded(currentSerial: Int) {
        guard !(serials.isEmpty) else { return }
        
        DispatchQueue.global().async { [weak self] in
            guard let strongSelf = self else { return }
            
            let holesToload = strongSelf.findNearestEmptyHole(serial: currentSerial)
            
            if let earlier = holesToload.earlier {
                let range = max(1, earlier - strongSelf.pageSize - 1)...(earlier - 1)
                strongSelf.loadRange(range)
            }
            
            if let later = holesToload.later {
                let rightBound = min(later + strongSelf.pageSize + 1,
                                     (strongSelf.serials.last ?? Int.max))
                let range = min(later + 1, rightBound)...rightBound
                strongSelf.loadRange(range)
            }
        }
    }
    
    //MARK: - Private functions
    private func loadUnread() {
        if let chat = Chat.get(id: chatId), chat.unreadCount > pageSize,
            let lastSerial = chat.lastMessage?.serial {
            loadIfNeeded(currentSerial: lastSerial - chat.unreadCount - 2)
        }
    }
    
    private func subscribeForUpdates() {
        dbStorage.notifyBG(
            MessageDTOBuilder.self,
            filter: [
                NSPredicate(format: "chat.id == %ld", chatId),
                NSPredicate(format: "id >= %ld", 0)
            ],
            sort: [("date", true)],
            tokenCreateBlock: { [weak self] (token) in self?.notificationToken = token }
        ) { [weak self] (changes) in
            switch changes {
            case .error(let error):
                Log.error("\(error)")
                return
            case .initial(let result):
                self?.serials = result.map { $0.serial }
                let lastSerial = result.last?.serial ?? 0
                self?.lastSerial = lastSerial
                self?.checkHoles()
                if lastSerial > 0 {
                    self?.loadIfNeeded(currentSerial: lastSerial)
                }
                self?.loadUnread()
            case .update(let result, _, let insertions, _):
                let serials: [Int] = result.enumerated()
                    .filter { insertions.contains($0.offset) }
                    .compactMap { $0.element?.serial }
                    .sorted() { $0 < $1 }
                if let lastSerial = serials.last {
                    self?.addHoleAtEndIfNeeded(lastInsertedSerial: lastSerial)
                }
                self?.closeHoles(serials: serials)
            }
        }
    }
    
    private func unsubscribe() {
        notificationToken?.stop()
        notificationToken = nil
    }
    
    private func loadRange(_ range: CountableClosedRange<Int>) {
        setStateIfNeeded(range: range, state: .loading)
        MessageRangeTask(chatId: chatId,
                         lowerSerial: range.lowerBound,
                         upperSerial: range.upperBound,
                         read: false)
            .success { [weak self] (_, result) in
                let messageSerials = result ?? []
                if messageSerials.count < range.count {
                    let loadedSet = Set(messageSerials)
                    let missed = Set(range).subtracting(loadedSet)
                    
                    self?.loadedEmptySerials.formUnion(missed)
                }
            }
            .error { [weak self] _ in
                self?.setStateIfNeeded(range: range, state: .failed)
            }
            .execute()
    }
    
    typealias SerialsNearHole = (earlier: Int?, later: Int?)
    private func findNearestEmptyHole(serial: Int) -> SerialsNearHole {
        var distanceEarlier = Int.max
        var distanceLater = Int.max
        for hole in holes {
            guard hole.state != .loading else { continue }
            
            let earlierItemDistance = serial - hole.range.upperBound
            let laterItemDistance = hole.range.lowerBound - serial
            
            if (0...pageSize).contains(earlierItemDistance), distanceEarlier > earlierItemDistance {
                distanceEarlier = earlierItemDistance
            }
            
            if (0...pageSize).contains(laterItemDistance), distanceLater > laterItemDistance {
                distanceLater = laterItemDistance
            }
        }
        
        return (earlier: distanceEarlier > pageSize ? nil : serial - distanceEarlier + 1,
                later: distanceLater > pageSize ? nil : serial + distanceLater - 1)
    }
    
    private func setStateIfNeeded(range: CountableClosedRange<Int>, state: Hole.State) {
        var newHoles = [Hole]()
        for hole in holes {
            if hole.state != state,
                let intersect = hole.range.intersect(other: range) {
                
                if intersect.lowerBound > hole.range.lowerBound {
                    let range = (hole.range.lowerBound)...(intersect.lowerBound - 1)
                    newHoles.append(Hole(range: range, state: hole.state))
                }
                
                newHoles.append(Hole(range: intersect, state: state))
                
                if intersect.upperBound < hole.range.upperBound {
                    let range = (intersect.upperBound + 1)...(hole.range.upperBound)
                    newHoles.append(Hole(range: range, state: hole.state))
                }
            } else {
                newHoles.append(hole)
            }
        }
        holes = newHoles
    }
    
    private func checkHoles() {
        holes = [Hole]()
        guard let last = serials.last
        else { return }
        
        let range = 1...last
        let split = range.split { serials.contains($0) || loadedEmptySerials.contains($0) }
            .map { range[$0.startIndex]...range[range.index(before: $0.endIndex)] }
        holes = split.map { Hole(range: $0, state: .empty) }

    }
    
    private func addHoleAtEndIfNeeded(lastInsertedSerial: Int) {
        if (lastInsertedSerial - self.lastSerial) > 1 {
            let range = (self.lastSerial + 1)...(lastInsertedSerial - 1)
            let hole = Hole(range: range, state: .empty)
            holes.append(hole)
        }
        
        self.lastSerial = max(lastInsertedSerial, self.lastSerial)
    }
    
    private func closeHoles(serials: [Int]) {
        let newHoles = holes.flatMap { rebuildHole(hole: $0, insertion: serials) }
        holes = newHoles
    }
    
    private func rebuildHole(hole: Hole, insertion: [Int]) -> [Hole] {
        guard !insertion.isEmpty else { return [hole] }
        
        let range = hole.range
        let split = range.split { insertion.contains($0) }
            .map { range[$0.startIndex]...range[range.index(before: $0.endIndex)] }
        
        return split.map { Hole(range: $0, state: hole.state) }
    }
}
