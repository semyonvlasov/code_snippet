//
//  BWDTimelineItem.h
//  testTimeline
//
//  Created by adm on 21.12.15.
//  Copyright © 2015 adm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BWDTimeline.h"
#import "BWDRecordedPeriodsCacheItem.h"

typedef enum {
    BWDTimelineItemOffsetAlignRight,
    BWDTimelineItemOffsetAlignLeft
} BWDTimelineItemOffsetAlign;


@class BWDTimelineContentView;
@class BWDTimelineScaleView;
@class BWDTimeline;
@class BWDTimelineItem;
@protocol BWDTimelineDelegate;



@protocol BWDTimeLineItemDelegate <UIScrollViewDelegate>

@optional
- (void)timelineItem:(BWDTimelineItem *)timeline ItemDidSetPointerDate:(NSDate *)pointerDate;
- (void)timelineItem:(BWDTimelineItem *)timeline ItemDidScrollPointerDate:(NSDate *)pointerDate;

@end


static float const BWDTimelineHorizontalGaps = 18;
static float const BWDTimelineItemScaleHeight = 20;
//static float const BWDTimelineItemContentVerticalGaps = 0;


@interface BWDTimelineItem : UIScrollView;

@property(nonatomic,assign) BWDTimelineItemOffsetAlign offsetAlign;

@property(nonatomic, weak) id<BWDTimeLineItemDelegate> delegate;

@property(nonatomic,strong, readonly) UIView *zoomView;

@property(nonatomic, assign) BWDTimelineDataState dataState;

@property(nonatomic, strong) BWDRecordedPeriodsCacheItem *dataItem;


@property(nonatomic, strong) UIColor *backgroundColor;
@property(nonatomic, strong) UIColor *contentColor;
@property(nonatomic, strong) UIColor *scaleColor;
@property(nonatomic, strong) UIFont *scaleFont;


- (void)refreshContentWithData:(NSArray *)data forDate:(NSDate *)timelineDate;
- (void)resizeContentWithScale:(float)scale;
- (void)zoomToPoint:(CGPoint)zoomPoint withScale:(CGFloat)scale animated:(BOOL)animated;

- (void)zoomToArchivePointerWithScale:(CGFloat)scale animated:(BOOL)animated;

- (void)updatePointVisibilityWithDate:(NSDate *)date;


- (NSDate *)dateNearestToPointX:(float)x;

@end
