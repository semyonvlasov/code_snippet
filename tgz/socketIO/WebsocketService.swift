//
//  Websocket.swift
//  togezzer-ios
//
//  Created by semyonvlasov on 25/07/2017.
//  Copyright © 2017 tgz. All rights reserved.
//

import Foundation
import SocketIO
import JSONCodable
import RealmSwift


protocol WebsocketServiceObserver: class {
    func onConnect()
    func onMessage(response: JSONObject)
    func onDisconnect(error: NSError?)
}

extension WebsocketServiceObserver {
    func onConnect() {}
    func onMessage(response: JSONObject) {}
    func onDisconnect(error: NSError?) {}
}

class WebsocketService {
    
    //MARK: - Injected properties
    weak var authChecker: IAuthChecker!
    weak var statistics: IStatisticsClient!
    
    //MARK: - Properties
    let defaultNameSpace = "togezzer.ios"
    
    var manager: SocketManager?
    var timeout: Int { get { return 1 } }
    
    var isConnected = false
    var socketIsConnected: Bool { return manager?.status == .connected }
    
    //MARK: - Statistics params
    var attemptCount = 1
    var connectionStartTime = Date().timeIntervalSince1970
    
    //MARK: - Observers
    private struct WeakObserver {
        weak var value: WebsocketServiceObserver?
    }
    
    private var observerList = [ObjectIdentifier: WeakObserver]()
    var observers: [WebsocketServiceObserver] {
        return observerList.values.compactMap { $0.value }
    }
    
    //MARK: - Interface
    func connect() {
        guard !socketIsConnected else { return }
        
        connectionStartTime = Date().timeIntervalSince1970
        attemptCount = 1
        
        statistics.log(
            .websocketConnectig,
            params: [.attemptCount: attemptCount]
        )
        
        let manager = self.manager ?? createSocket()
        Log.info("🌓 Websocket connecting...")
        isConnected = true
        manager.connect()
    }

    func disconnect() {
        guard let manager = self.manager else { return }
        
        isConnected = false
        manager.disconnect()
    }
    
    func resetManager() {
        if socketIsConnected {
            disconnect()
        }
        manager = nil
    }
    
    func addObserver(_ observer: WebsocketServiceObserver) {
        observerList[ObjectIdentifier(observer)] = WeakObserver(value: observer)
        if socketIsConnected {
            observer.onConnect()
        }
    }
    
    func removeObserver(_ observer: WebsocketServiceObserver) {
        observerList[ObjectIdentifier(observer)] = nil
    }
    
    private func websocketDidConnect() {
        Log.info("🌞 Websocket connected")
        
        statistics.log(
            .websocketConnected,
            params: [
                .attemptCount: attemptCount,
                .time: Date().timeIntervalSince1970 - connectionStartTime
            ]
        )
        
        observers.forEach { $0.onConnect() }
    }
    
    private func websocketDidDisconnect() {
        Log.info("🌚 Websocket disconnected")
        observers.forEach { $0.onDisconnect(error: nil) }
    }
    
    private func getHeaders() -> [String: String] {
        var headers = [String: String]()
        headers ["User-Agent"] = PreferencesHelper.getClientInfo()

        if let token = authChecker.getAuthToken() {
            headers["Authorization"] = "Bearer \(token)"
        }
        
        return headers
    }
    
    private func createSocket() -> SocketManager {
        let headers = getHeaders()
        let configElement = SocketIOClientConfiguration(
            arrayLiteral:
            .path("/api/ws/"),
            .extraHeaders(headers),
            .reconnects(true),
            .reconnectAttempts(-1),
            .reconnectWait(self.timeout)
        )
        let wsUrl = PreferencesHelper.getBackendUrl()
        let socketManager = SocketManager(socketURL: URL(string: wsUrl)!, config: configElement)
        self.manager = socketManager
        let socketIO = socketManager.defaultSocket
        
        socketIO.on(clientEvent: .connect) { [weak self] (_, _) in
            self?.websocketDidConnect()
        }
        socketIO.on(clientEvent: .disconnect) { [weak self] (_, _) in
            self?.websocketDidDisconnect()
        }
        socketIO.on(clientEvent: .reconnectAttempt) { [weak self] (_, _) in
            self?.attemptCount += 1
            
            self?.statistics.log(
                .websocketConnectig,
                params: [.attemptCount: self?.attemptCount ?? 0]
            )
            
            Log.info("🌓 Try to reconnect websocket")
        }
        socketIO.on(clientEvent: .error) { [weak self] (_, _) in
            Log.error("Websocket service: socket connection error")
            
            guard
                let attempt = self?.attemptCount,
                let started = self?.connectionStartTime
            else { return }
            
            self?.statistics.log(
                .websocketConnectionFailed,
                params: [
                    .attemptCount: attempt,
                    .time: Date().timeIntervalSince1970 - started
                ]
            )
        }
        
        socketIO.onAny { [weak self] event in
            for item in event.items! {
                if let itemString = item as? String {
                    if let data = itemString.data(using: .utf8) {
                        do {
                            guard let response = try JSONSerialization
                                .jsonObject(with: data, options: []) as? JSONObject else {
                                    return
                            }
                            let type = (response["action"] as? String) ?? "unknown"
                            let logMessage = "Websocket message received, type: \(type)"
                            Log.info("📩 " + logMessage)
                            Log.verbose(logMessage + "\n \(response.json())")
                            self?.observers.forEach { $0.onMessage(response: response) }
                        }
                        catch { return }
                    }
                }
            }
        }
        return socketManager
    }
}
