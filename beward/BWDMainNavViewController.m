//
//  BWDMainNavViewController.m
//  camDrive
//
//  Created by adm on 27.11.15.
//  Copyright © 2015 Semyon Vlasov All rights reserved.
//

#import "BWDMainNavViewController.h"
#import "BWDLeftViewController.h"
#import "BWDSignInViewController.h"

@interface BWDMainNavViewController ()
    @property (strong, nonatomic) BWDLeftViewController *leftViewController;

@end

@implementation BWDMainNavViewController {
    UIViewController *_lastViewController;
}
    
static NSString * const kDefaultVCId = /*@"storyIdPlayerWrapperVC";*/ @"storyIdVideoNC";

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    self.view.backgroundColor = [UIColor whiteColor];
    
    _leftViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"storyIdLeft"];
    self.rootViewController = [[BWDApiManager sharedInstance] isAuthorized] ? [self menuVCWithId:nil] : [self signInVC];
    [self setLeftViewEnabledWithWidth:250.f
                    presentationStyle:LGSideMenuPresentationStyleSlideAbove
                 alwaysVisibleOptions:0];
    
    _leftViewController.view.backgroundColor = [UIColor clearColor];
    _leftViewController.tableView.backgroundColor = [UIColor clearColor];
    self.leftViewBackgroundColor = UIColorFromRGB(0x5284E5);

    [self.leftView addSubview:_leftViewController.view];
    
    self.leftViewStatusBarVisibleOptions = LGSideMenuStatusBarVisibleOnPadLandscape |
    LGSideMenuStatusBarVisibleOnPadPortrait |
    LGSideMenuStatusBarVisibleOnPhoneLandscape |
    LGSideMenuStatusBarVisibleOnPhonePortrait;
    self.leftViewSwipeGestureEnabled = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotif:) name:notif_Authorized object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotif:) name:notif_NeedSignIn object:nil];
}


- (UIViewController *)signInVC {
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"storyIdSignIn"];
    self.shouldShowLeftView = NO;
    return vc;
}

- (UIViewController *)menuVCWithId:(NSString *)vcStoryId {
    UIViewController *vc;
    if (vcStoryId) {
        vc = [self.storyboard instantiateViewControllerWithIdentifier:vcStoryId];
    }
    else { // view controller by default
        vc = [self.storyboard instantiateViewControllerWithIdentifier:kDefaultVCId];
        //[(UINavigationController *)vc setNavigationBarHidden:NO animated:NO];
    }
    self.shouldShowLeftView = YES;
    return vc;
}

- (void)receiveNotif:(NSNotification *)notif {
    if ([notif.name isEqualToString:notif_NeedSignIn]) {
        if (![self.rootViewController isKindOfClass:[BWDSignInViewController class]]) {
            _lastViewController = self.rootViewController;
            self.rootViewController = [self signInVC];
        }
    }
    if ([notif.name isEqualToString:notif_Authorized]) {
        self.rootViewController = _lastViewController ? _lastViewController : [self menuVCWithId:nil];
        _lastViewController = self.rootViewController;
    }
}

- (void)leftViewWillLayoutSubviewsWithSize:(CGSize)size
{
    [super leftViewWillLayoutSubviewsWithSize:size];

    
    CGFloat statusBarHeight;
    if([[UIApplication sharedApplication] isStatusBarHidden]) {
        statusBarHeight = 20.0f;
    } else {
        statusBarHeight = 0.f;
    }
    
    _leftViewController.view.frame = CGRectMake(0.f , statusBarHeight, size.width, size.height - statusBarHeight);
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldShowLeftView {
    self.leftView.backgroundColor = UIColorFromRGB(0x5284E5);
    [_leftViewController updateUserInfo];
    return YES;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



@end
