//
//  String+RealmPrimitivesArray.swift
//  togezzer-ios
//
//  Created by Семен Власов on 27/06/2019.
//  Copyright © 2019 tgz. All rights reserved.
//

import Foundation

extension String {
    
    //MARK: - Static
    private static let rlmSeparator = "#"
    
    static let rlmEmptyIds = ""
    
    static func rlmIds(_ ids: [Int]?) -> String {
        guard let ids = ids else { return .rlmEmptyIds }
        return ids.reduce("") { $0 + rlmId($1) }
    }
    
    static func rlmId(_ id: Int) -> String {
        return rlmSeparator + "\(id)" + rlmSeparator
    }
    
    //MARK: - Common
    var rlmIds: [Int] {
        return components(separatedBy: String.rlmSeparator).compactMap { Int($0) }
    }
    
    mutating func rlmAppend(id: Int) {
        self = rlmAppending(id: id)
    }
    
    mutating func rlmAppendUnique(id: Int) {
        self = rlmAppendingUnique(id: id)
    }
    
    mutating func rlmRemove(id: Int) {
        self = rlmRemoving(id: id)
    }
    
    func rlmAppending(id: Int) -> String {
        return self + String.rlmId(id)
    }
    
    func rlmAppendingUnique(id: Int) -> String {
        if  rlmContains(id: id) {
            return self
        } else {
            return rlmAppending(id: id)
        }
    }
    
    func rlmRemoving(id: Int) -> String {
        guard let range = self.range(of: String.rlmId(id)) else { return self }
        
        var ret = self
        ret.removeSubrange(range)
        
        return ret
    }
    
    func rlmContains(id: Int) -> Bool {
        return range(of: String.rlmId(id)) != nil
    }
}
