//
//  ObjectDTO.swift
//  togezzer-ios
//
//  Created by Семен Власов on 13.09.2018.
//  Copyright © 2018 tgz. All rights reserved.
//

import Foundation
import RealmSwift

enum DBItemSyncStatus {
    case success
    case syncing(taskId: String?)
    case fail
    
    init(model: RealmModel, taskId: String? = nil) {
        if model.isSyncError { self = .fail }
        else if model.isSync { self = .success }
        else { self = .syncing(taskId: taskId) }
    }
}

extension DBItemSyncStatus: Equatable {
    static func == (lhs: DBItemSyncStatus, rhs: DBItemSyncStatus) -> Bool {
        switch (lhs, rhs) {
        case (.success, .success), (.fail, .fail): return true
        case (.syncing(let v_), .syncing(let _v)): return _v == v_
        default: return false
        }
    }
    
    var isSyncing: Bool { return self != .success && self != .fail }
}

protocol DTObjectProtocol {
    var id: Int { get }
    var syncStatus: DBItemSyncStatus { get }
}

struct DTObject: DTObjectProtocol {
    let id: Int
    let syncStatus: DBItemSyncStatus
}

protocol DecoratedDTObjectProtocol: DTObjectProtocol {
    var baseDTO: DTObjectProtocol { get }
}

extension DecoratedDTObjectProtocol {
    var id: Int { return baseDTO.id }
    var syncStatus: DBItemSyncStatus { return baseDTO.syncStatus }
}

enum DBLink<DTO: DTObjectProtocol>: Hashable {
    
    case empty(id: Int)
    case exists(model: DTO)
    
    public var hashValue: Int { return id.hashValue }
    
    var model: DTO? {
        if case .exists(let model) = self {
            return model
        } else { return nil }
    }
    
    var id: Int {
        switch self {
        case .empty(let id): return id
        case .exists(let model): return model.id
        }
    }
    
    var isEmpty: Bool {
        if case .empty = self {
            return true
        } else {
            return false
        }
    }
    
    static func == (lhs: DBLink<DTO>, rhs: DBLink<DTO>) -> Bool {
        return lhs.id == rhs.id
    }
    
    init(nullableDTO: DTO? = nil, id: Int) {
        if let dto = nullableDTO {
            self = .exists(model: dto)
        } else {
            self = .empty(id: id)
        }
    }
    
    init(dto: DTO) {
        self = .exists(model: dto)
    }
}

class DTObjectBuilder<DTO: DTObjectProtocol, DBO: RealmModel> {
    class func merge(dtObject: DTO, to dbObject: DBO) -> DBO {
        assertionFailure("Override in subclass")
        return dbObject
    }
    
    class func getLink(id: Int, realm: Realm? = nil) -> DBLink<DTO> {
        guard
            let dbObject = DBO.get(id: id),
            let realm = realm ?? (try? Realm()),
            let dto = translate(dbObject: dbObject, realm: realm)
        else { return .empty(id: id) }
        
        return .exists(model: dto)
    }
    
    class func getLink(nullableObject: DBO?, realm: Realm? = nil) -> DBLink<DTO>? {
        guard let dbObject = nullableObject else { return nil }
        
        guard
            let realm = realm ?? (try? Realm()),
            let dto = translate(dbObject: dbObject, realm: realm)
        else { return .empty(id: dbObject.id) }
        
        return .exists(model: dto)
    }
    
    class func getLink(dbObject: DBO, realm: Realm) -> DBLink<DTO> {
        guard
            let dto = translate(dbObject: dbObject, realm: realm)
        else { return .empty(id: dbObject.id) }
        
        return .exists(model: dto)
    }
    
    class func get(id: Int?) -> DTO? {
        return get(id: id, realm: nil)
    }
    
    class func get(id: Int?, realm: Realm? = nil) -> DTO? {
        guard
            let id = id,
            let dbObject = DBO.get(id: id),
            let realm = realm ?? (try? Realm()),
            let dto = translate(dbObject: dbObject, realm: realm)
        else { return nil }
        
        return dto
    }
    
    class func getNullableLink(id: Int?, realm: Realm? = nil) -> DBLink<DTO>? {
        guard let id = id, id > 0 else { return nil }
        return getLink(id: id, realm: realm)
    }
    
    class func translate(dbUnwrapped: DBO?, realm: Realm? = nil) -> DTO? {
        guard
            let dbObject = dbUnwrapped,
            let realm = realm ?? (try? Realm())
        else { return nil }
        return translate(dbObject: dbObject, realm: realm)
    }
    
    class func translate(dbObject: DBO, realm: Realm) -> DTO? {
        guard !dbObject.isEmpty else { return nil }
        
        return translateNonEmpty(dbObject: dbObject, realm: realm)
    }
    
    class func translateNonEmpty(dbObject: DBO, realm: Realm) -> DTO? {
        assertionFailure("Override in subclass")
        return nil
    }
    
    final class func getBaseDTO(dbObject: DBO) -> DTObjectProtocol? {
        guard !dbObject.isEmpty else { return nil }
        
        let syncStatus = DBItemSyncStatus(model: dbObject, taskId: nil)
        return DTObject(id: dbObject.id, syncStatus: syncStatus)
    }
}
