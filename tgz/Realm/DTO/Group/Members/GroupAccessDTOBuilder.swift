//
//  GroupAccessDTOBuilder.swift
//  togezzer-ios
//
//  Created by Семен Власов on 04/10/2019.
//  Copyright © 2019 tgz. All rights reserved.
//

import RealmSwift

class GroupAccessDTOBuilder: DTObjectBuilder<GroupAccessDTO, Group> {
    
    override class func translateNonEmpty(dbObject: Group, realm: Realm) -> GroupAccessDTO? {
        guard
            let shortDTO = ShortGroupDTOBuilder.translate(dbObject: dbObject, realm: realm)
        else {
            Log.error("GroupDTOBuilder Can't create dto from: \(dbObject)")
            return nil
        }
        

        return GroupAccessDTO(
            shortGroupDTO: shortDTO,
            members: dbObject.members
                .compactMap(curry(GroupMemberDTOBuilder.translate)(realm))
        )
    }
}
