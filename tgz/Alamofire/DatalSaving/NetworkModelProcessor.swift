//
//  NetworkModelProcessor.swift
//  togezzer-ios
//
//  Created by Семен Власов on 19/11/2018.
//  Copyright © 2018 tgz. All rights reserved.
//

import Foundation
import RealmSwift

protocol NetworkModelProcessor {
    associatedtype Builder: NetworkModelBuilder
    associatedtype Saver: DBModelSaver where Saver.Model == Builder.Model
}

extension NetworkModelProcessor {
    typealias Model = Builder.Model
    typealias DBO = Saver.DBO
}
