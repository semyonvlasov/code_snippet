//
//  NetworkModelBuilder.swift
//  togezzer-ios
//
//  Created by Семен Власов on 06/08/2019.
//  Copyright © 2019 tgz. All rights reserved.
//

import Foundation
import JSONCodable

protocol NetworkModel {
    var id: Int { get }
}

protocol NetworkModelBuilder {
    associatedtype Model: NetworkModel
    
    static func createModel(nullableJson: JSONObject?) -> Model?
    static func createModel(from json: JSONObject) -> Model?
}

extension NetworkModelBuilder {
    static func createModel(nullableJson: JSONObject?) -> Model? {
        guard let json = nullableJson else { return nil }
        return createModel(from: json)
    }
}
