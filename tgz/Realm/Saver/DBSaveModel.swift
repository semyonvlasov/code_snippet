//
//  DBSaveModel.swift
//  togezzer-ios
//
//  Created by Семен Власов on 06/08/2019.
//  Copyright © 2019 tgz. All rights reserved.
//

import Foundation

protocol DBSaveModel {
    var internalId: Int? { get }
    var remoteId: Int? { get }
}

extension NetworkModel {
    //TODO: для внутренних айди и оффлайна
    var internalId: Int? { return nil }
    var remoteId: Int? { return id }
}
