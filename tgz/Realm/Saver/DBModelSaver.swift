//
//  DBModelSaver.swift
//  togezzer-ios
//
//  Created by Семен Власов on 06/08/2019.
//  Copyright © 2019 tgz. All rights reserved.
//

import Foundation
import RealmSwift

protocol DBModelSaver {
    associatedtype Model: DBSaveModel
    associatedtype DBO: RealmModel
    
    static func merge(model: Model, dbObject: DBO, realm: Realm)
    
    //Для линка созданного объекта ко внешним объектам
    static func linkExternal(model: Model, dbObject: DBO, realm: Realm)
}

extension DBModelSaver {
    static func linkExternal(model: Model, dbObject: DBO, realm: Realm) {}
}
