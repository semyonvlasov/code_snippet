//
//  TableUpdateDefault.swift
//  togezzer-ios
//
//  Created by Семен Власов on 30.05.2018.
//  Copyright © 2018 tgz. All rights reserved.
//

import UIKit

protocol TableUpdateDefault {
    var tableView: UITableView! { get }
}

extension TableUpdateDefault {
    
    func performBatchUpdates(_ changes: CollectionChanges,
                             updateType: CollectionUpdateType,
                             beforeUpdateBlock: @escaping () -> Void) {
        let wantsReloadData = updateType == .firstLoad
        
        if changesIsEmpty(changes) {
            beforeUpdateBlock()
        } else if !wantsReloadData {
            tableView.setEditing(false, animated: true)
            tableView.beginUpdates()
            
            beforeUpdateBlock()
            
            tableView.deleteSections(changes.removedSections, with: .automatic)
            tableView.deleteRows(at: Array(changes.deletedIndexPaths) as [IndexPath],
                                 with: .automatic)
            
            tableView.insertSections(changes.insertedSections, with: .automatic)
            tableView.insertRows(at: Array(changes.insertedIndexPaths) as [IndexPath],
                                 with: .automatic)
            changes.movedIndexPaths
                .forEach { tableView.moveRow(at: $0.indexPathOld, to: $0.indexPathNew) }
            
            tableView.endUpdates()
        } else {
            beforeUpdateBlock()
            tableView.reloadData()
        }
        
        if updateType == .reload {
            tableView.endRefreshing()
        }
    }
    
    private func changesIsEmpty(_ changes: CollectionChanges) -> Bool {
        return changes.deletedIndexPaths.isEmpty &&
            changes.insertedIndexPaths.isEmpty &&
            changes.movedIndexPaths.isEmpty &&
            changes.insertedSections.isEmpty &&
            changes.removedSections.isEmpty &&
            !changes.itemsContentChanged
        
    }
    
    func reloadData() {
        tableView?.reloadData()
    }
    
    var collectionTypeView: CollectionTypeViewProtocol {
        return tableView
    }
}
