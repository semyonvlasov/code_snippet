//
//  UIView+Appearance.h
//  PhotoViewer
//
//  Created by Semyon Vlasov on 27.07.15.
//  Copyright (c) 2015 SVV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Appearance)

- (void)addSubviewOverlaySuperView:(UIView *)subview;

@end
