//
//  CollectionItemCompanion.swift
//  togezzer-ios
//
//  Created by semyonvlasov on 18/01/2018.
//  Copyright © 2018 tgz. All rights reserved.
//

import Foundation

struct CollectionItemCompanion: Uniquelyable {
    
    let uid: String
    let item: CollectionModelProtocol
    let presenter: CollectionItemPresenterProtocol
}
