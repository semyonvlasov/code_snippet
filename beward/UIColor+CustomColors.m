//
//  UIColor+CustomColors.m
//  camDrive
//
//  Created by adm on 16.05.16.
//  Copyright © 2016 adm. All rights reserved.
//

#import "UIColor+CustomColors.h"

@implementation UIColor (CustomColors)

+ (UIColor *)BWDGrayColor { return UIColorFromRGB(0xa0a0a0); }
//+ (UIColor *)BWDDarkGrayColor { return UIColorFromRGB(0x767676); }
+ (UIColor *)BWDLightGrayColor { return UIColorFromRGB(0xe0e0e0); }

+ (UIColor *)BWDDarkGrayFontColor { return UIColorFromRGB(0x767676); }
+ (UIColor *)BWDGrayFontColor { return UIColorFromRGB(0xa0a0a0); }
+ (UIColor *)BWDLightGrayFontColor { return UIColorFromRGB(0xc6c6cb); }

+ (UIColor *)BWDBlueColor { return UIColorFromRGB(0x35b3ec); }
+ (UIColor *)BWDRedColor { return UIColorFromRGB(0xff2d55); }
+ (UIColor *)BWDGreenColor { return UIColorFromRGB(0x90d849); }
+ (UIColor *)BWDOrangeColor { return UIColorFromRGB(0xe59222); }

@end
