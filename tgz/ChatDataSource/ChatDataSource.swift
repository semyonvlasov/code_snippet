//
//  ChatDataSource.swift
//  togezzer-ios
//
//  Created by Семен Власов on 26.06.2018.
//  Copyright © 2018 tgz. All rights reserved.
//

import RealmSwift

class ChatDataSource: CollectionDataSourceProtocol {

    //MARK: - Properties
    var chatId: Int
    weak var interactor: ChatInteractorInput?
    var dbStorage: DBStorage
    var userSettings: IUserSettingsProvider
    
    weak var delegate: CollectionDataSourceDelegateProtocol?
    private var earliestSerial = Int.max
    var hasMorePrevious: Bool {
        return earliestSerial > 1
    }
    
    var unreadAdded: Bool { return (getFirstUnreadSerial(chatId: chatId) ?? 0) > 0 }
    private var unreadSerial: Int?
    
    private var previewLoader = MessagePreviewLoader()
    
    private var syncToken: DBNotificationToken?
    private var nonSyncToken: DBNotificationToken?
    private var lastSynced: MessageDTO?
 
    private var messages = [MessageModelProtocol]()
    private var nonSync = [MessageModelProtocol]()
    
    private var worker: BackgroundWorker
    
    private var holeDetector: ChatHoleDetector
    private let holeLoadDelay: Double = 0.5
    private var holeLoadDelayTimer: Timer?
    private var currentShownSerial: Int?
    
    private let updateDelay: Double = 0.5
    private var updateDelayTimer: Timer?
    private var updateType: CollectionUpdateType?
    
    private let pageSize = 50
    
    //MARK: - Lifecycle
    required init(chatId: Int,
                  storageService: IStorage,
                  interactor: ChatInteractorInput,
                  dbStorage: DBStorage,
                  userSettings: IUserSettingsProvider) {
        self.worker = BackgroundWorker()
        self.worker.start()
        self.chatId = chatId
        self.interactor = interactor
        self.dbStorage = dbStorage
        self.userSettings = userSettings
        self.holeDetector = ChatHoleDetector(
            chatId: chatId,
            storageService: storageService,
            dbStorage: dbStorage
        )
        getFirstUnreadSerial(chatId: chatId)
    }
    
    required init() { fatalError("not implemented") }
    
    deinit {
        removeSubscriptions()
    }
    
    //MARK: Interface
    func checkForLoad(currentSerial: Int) {
        currentShownSerial = currentSerial
        if holeLoadDelayTimer == nil {
            loadDelayTimerExpired()
        }
    }
    
    func removeUnread() {
        unreadSerial = -1
        setNeedsUpdate(type: .update)
    }
    
    func reload() {
        let firstLoad = messages.isEmpty
        messages = []
        
        let lastMessageSerial = Chat.get(id: chatId)?.lastMessage?.serial ?? 1
        
        earliestSerial = min(earliestSerial, max(lastMessageSerial - pageSize, 1))
        
        guard lastMessageSerial >= earliestSerial else { return }
        
        loadInRange(earliestSerial...lastMessageSerial,
                    updateType: firstLoad ? .firstLoad : .reload)
    }
    
    func getUid(serial: Int) -> String? {
        return messages.first { $0.serial == serial }?.uid
    }
    
    func loadToUnread() {
        guard let firstUnreadSerial = getFirstUnreadSerial(chatId: chatId) else { return }
        loadToSerial(serial: firstUnreadSerial)
    }
    
    func loadToSerial(serial: Int) {
        loadPrevious(newEarliest: serial)
        holeDetector.loadIfNeeded(currentSerial: serial)
    }
    
    func loadPrevious() {
        loadPrevious(newEarliest: (earliestSerial) - pageSize)
    }
    
    func addSubscriptions() {
        removeSubscriptions()
        
        dbStorage.notifyBG(
            MessageDTOBuilder.self,
            filter: [
                NSPredicate(format: "chat.id == %ld", chatId),
                NSPredicate(format: "id >= %ld", 0)
            ],
            sort: [("date", true)],
            tokenCreateBlock: { [weak self] (token) in self?.syncToken = token }
        ) { [weak self] (changes) in
            switch changes {
            case .error(let error): Log.error(error)
            case .initial(let result):
                self?.lastSynced = result.last
            case .update(let result, _, let insert, let update):
                if let last = result.last {
                    self?.lastSynced = last
                }
                
                let insertIndexes = insert + update
                let bounds = insertIndexes.reduce((Int.max, 0)) {
                    return (min($0.0, $1), max($0.1, $1))
                }
                
                //Из-за LoadMembersTask прилетает апдейт для всех загруженных сообщений и потому
                //они мержатся все при открытии чата
                //Из-за этого апдейта earliestSerial некорректный и reload при возврате работает криво
                guard
                    bounds.0 <= bounds.1,
                    let leftSerial = self?.earliestSerial,
                    let rightSerial = result[safe: bounds.1]??.serial,
                    leftSerial <= rightSerial
                    else { return }
                
                let new = result.compactMap { $0 }.filter {
                    $0.serial >= leftSerial && $0.serial <= (rightSerial + 1)
                }
                self?.handleMessagesUpdate(update: new, updateType: .loadNew)
            }
        }
        
        dbStorage.notifyBG(
            MessageDTOBuilder.self,
            filter: [
                NSPredicate(format: "chat.id == %ld", chatId),
                NSPredicate(format: "id < %ld", 0)
            ],
            sort: [("date", true)],
            tokenCreateBlock: { [weak self] (token) in self?.nonSyncToken = token }
        ) { [weak self] (changes) in
            switch changes {
            case .error(let error): Log.error(error)
            case .initial(let result):
                self?.saveNonSyncMessages(dtos: result)
            case .update(let result, _, _, _):
                self?.saveNonSyncMessages(dtos: result.compactMap { $0 })
            }
        }
    }
    
    func removeSubscriptions() {
        syncToken?.stop()
        syncToken = nil
        
        nonSyncToken?.stop()
        nonSyncToken = nil
    }
    
    //MARK: - Private functions
    @objc private func loadDelayTimerExpired() {
        holeLoadDelayTimer?.invalidate()
        holeLoadDelayTimer = nil
        
        if let current = currentShownSerial {
            holeDetector.loadIfNeeded(currentSerial: current)
            currentShownSerial = nil
            holeLoadDelayTimer = Timer.scheduledTimer(timeInterval: holeLoadDelay,
                                                      target: self,
                                                      selector: #selector(loadDelayTimerExpired),
                                                      userInfo: nil,
                                                      repeats: false)
        }
    }

    private func handleMessagesUpdate(update: [MessageDTO],
                                      updateType: CollectionUpdateType) {
        mergeMessageModels(merge: update)

        worker.executeBlock { [weak self] in
            self?.updateNonSyncMessages()
            self?.setNeedsUpdate(type: updateType)
        }
    }

    private func mergeMessageModels(merge: [MessageDTO]) {
        let userId = userSettings.userId
        
        worker.executeBlock { [weak self] in
            let isFirst = (self?.messages.first?.serial ?? Int.max) > (merge.first?.serial ?? 0)
            let isNewLast = (self?.messages.last?.serial ?? 0) < (merge.first?.serial ?? Int.max)
            
            merge.forEach {
                self?.checkForNonDeleted(item: $0)
                self?.checkForEmptyPreviewLink(item: $0)
            }
            
            let new = merge.enumerated().compactMap {
                $0.offset == 0 && !isFirst && !isNewLast
                    ? nil
                    : ChatMessageProcessor.processMessageItem(
                        $0.element,
                        prev: merge[safe: $0.offset - 1],
                        currentUserId: userId
                )
            }
            
            let old = (self?.messages ?? []).filter { oldItem in
                !new.contains { newItem in newItem.serial == oldItem.serial }
            }
            
            let merged = (old + new).sorted { $0.serial < $1.serial }
            
            self?.messages = merged
            let imageMessages =  new.compactMap { $0 as? ImageMessageModelProtocol }
            self?.prefetchImages(messages: imageMessages)
        }
    }

    private func updateNonSyncMessages() {
        nonSync.removeAll { message -> Bool in
            messages.contains { message.uid == $0.uid }
        }
    }

    private func presentMessages(updateType: CollectionUpdateType) {
        var feed: [CollectionModelProtocol] = nonSync + createFeed(messages: messages)
        
        //Если фид пустой, то подсовываем пустое системное сообзение,
        //чтобы корректно отображался хидер чяйтика
        if feed.isEmpty {
            feed.append(ChatMessageProcessor.getEmptySystemMessage())
        }
        
        DispatchQueue.main.async { [weak self] in
            self?.delegate?.dataSourceDidUpdate(
                updateData: (items: feed,
                             sections: [SectionModel(items: feed.count, type: 0, title: "")]),
                updateType: updateType,
                completion: nil
            )
        }
    }
    
    private func saveNonSyncMessages(dtos: [MessageDTO]) {
        let userId = userSettings.userId
        
        worker.executeBlock { [weak self] in
            guard let strongSelf = self else { return }
            
            let nonSync = dtos.enumerated().compactMap {
                ChatMessageProcessor.processMessageItem(
                    $0.element,
                    prev: $0.offset > 0 ? dtos[safe: $0.offset - 1] : strongSelf.lastSynced,
                    currentUserId: userId
                )
            }
            let needUpdate = !strongSelf.nonSync.isEmpty || !nonSync.isEmpty
            
            strongSelf.nonSync = nonSync
            
            if needUpdate {
                strongSelf.setNeedsUpdate(type: .loadNew)
            }
        }
    }
    
    private func createFeed(messages: [MessageModelProtocol]) -> [CollectionModelProtocol] {
        var feed = [CollectionModelProtocol]()
        
        var prevSerial = 0
        var prevDate = Date(timeIntervalSince1970: 0)
        
        let firstUnreadSerial: Int? = getFirstUnreadSerial(chatId: chatId)
        var hasUnread = false
        
        messages.sorted { sortMessages(l: $0, r: $1) }.forEach {
            var shouldAddUnreadDelimeter = false
            
            if let unread = firstUnreadSerial, !hasUnread {
                if unread == $0.serial  {
                    shouldAddUnreadDelimeter = true
                } else if unread < $0.serial {
                    hasUnread = true
                    feed.append(ChatMessageProcessor.createUnreadDelimeter())
                }
            }
            
            if ($0.serial - prevSerial) > 1 {
                let emptySet = Set((prevSerial + 1)...($0.serial - 1))
                    .subtracting(holeDetector.loadedEmptySerials)
                if !emptySet.isEmpty {
                    feed.append(ChatMessageProcessor.createLoader(before: $0.serial))
                }
            }
            
            if !$0.date.compare(.isSameDay(as: prevDate)) {
                prevDate = $0.date
                feed.append(ChatMessageProcessor.createDateDelimeter(date: prevDate))
            }
            
            prevSerial = $0.serial
            
            if shouldAddUnreadDelimeter {
                hasUnread = true
                feed.append(ChatMessageProcessor.createUnreadDelimeter())
            }
            
            feed.append($0)
        }
        return feed.reversed()
    }
    
    private func setNeedsUpdate(type: CollectionUpdateType) {
        updateType = calculateUpdateType(old: updateType, new: type)
        if updateDelayTimer == nil {
            updateDelayTimerExpired()
        }
    }
    
    //Вычисляем тип обновления для нескольких разнотипных обновлений
    private func calculateUpdateType(old: CollectionUpdateType?,
                                     new: CollectionUpdateType) -> CollectionUpdateType {
        guard let old = old else { return new }
        
        let priority: [CollectionUpdateType] = [.firstLoad, .reload, .pagination, .loadNew, .update]
        let index = min(priority.index(of: old) ?? 0, priority.index(of: new) ?? 0)
        
        return priority[index]
    }
    
    @objc func updateDelayTimerExpired() {
        updateDelayTimer?.invalidate()
        updateDelayTimer = nil
        
        if let updateType = self.updateType {
            self.updateType = nil
            updateDelayTimer = Timer.scheduledTimer(
                timeInterval: updateDelay,
                target: self,
                selector: #selector(updateDelayTimerExpired),
                userInfo: nil,
                repeats: false
            )
            
            presentMessages(updateType: updateType)
        }
    }
    
    private func sortMessages(l: MessageModelProtocol, r: MessageModelProtocol) -> Bool {
        if l.serial < r.serial { return true }
        else if l.serial == r.serial { return l.date < r.date }
        else { return false }
    }
    
    private func loadPrevious(newEarliest: Int) {

        guard earliestSerial > 1, newEarliest < earliestSerial else { return }
        let oldEarliest = earliestSerial
        earliestSerial = newEarliest
        
        loadInRange((newEarliest - 1)...oldEarliest, updateType: .pagination)
    }
    
    private func loadInRange(_ range: CountableClosedRange<Int>,
                             updateType: CollectionUpdateType) {
        worker.executeBlock { [weak self] in
            guard let strongSelf = self else { return }
            let page = strongSelf.dbStorage.fetch(
                MessageDTOBuilder.self,
                filter: [
                    NSPredicate(format: "chat.id == %ld", strongSelf.chatId),
                    NSPredicate(format: "id >= %ld", 0),
                    NSPredicate(format: "serial >= %ld && serial <= %ld",
                                range.lowerBound,
                                range.upperBound)
                ],
                sort: [("date", true)]
            )
            
            strongSelf.handleMessagesUpdate(update: page, updateType: updateType)
        }
    }

    private func checkForEmptyPreviewLink(item: MessageDTO) {
        guard
            case .text(_, let preview) = item.type,
            let link = preview,
            link.syncStatus != .success,
            !link.url.absoluteString.isEmpty
        else { return }
        
        previewLoader.loadPreview(previewId: link.id, link: link.url.absoluteString)
    }
    
    private func checkForNonDeleted(item: MessageDTO) {
        if case .deleted = item.type,
            case .syncing(let taskId) = item.syncStatus,
            !TasksStorage.hasTask(stringId: taskId) {
            interactor?.deleteMessage(id: item.id)
        }
    }
    
    @discardableResult private func getFirstUnreadSerial(chatId: Int) -> Int? {
        if let unreadSerial = unreadSerial {
            return unreadSerial > 0 ? unreadSerial : nil
        }
        
        guard
            let chat = Chat.get(id: chatId),
            chat.unreadCount > 0
        else {
            unreadSerial = -1
            return nil
        }
        
        if let serial = Message.get(id: chat.firstUnreadId)?.serial {
            unreadSerial = serial
            return serial
        } else if let lastSerial = chat.lastMessage?.serial {
            return lastSerial - chat.unreadCount + 1
        }
        
        return nil
    }
    
    private func prefetchImages(messages: [ImageMessageModelProtocol]) {
        ImageLoader.shared.loadImagesToCache(imageUrls: messages.map { $0.imageUrl },
                                             compressImageSize: .full)
    }
}
