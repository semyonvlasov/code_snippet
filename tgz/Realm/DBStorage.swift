//
//  DBStorage.swift
//  togezzer-ios
//
//  Created by Семен Власов on 12.09.2018.
//  Copyright © 2018 tgz. All rights reserved.
//

import RealmSwift

enum DBChanges<DTO: DTObjectProtocol> {
    case error(Error)
    case initial(result: [DTO])
    case update(result: [DTO?], deletions: [Int], insertions: [Int], updates: [Int])
}

enum DBCountEvent {
    case error(Error)
    case initial(value: Int)
    case change(value: Int)
}

enum DBObjectChanges<DTO: DTObjectProtocol, DBO> {
    case error(Error)
    case deleted
    case change(dto: DTO, changes: [DBPropertyChange<DBO>], rlmChanges: [PropertyChange])
    case initial(dto: DTO)
}

final class DBStorage {
    private var mainThreadRealm: Realm!
    private var bgRealm: Realm!
    private(set) var worker = BackgroundWorker()
    private var started = false

    func startIfNeeded() {
        guard !started else { return }
        started = true
        
        worker.start()
        
        if Thread.isMainThread {
            mainThreadRealm = try! Realm()
        } else {
            DispatchQueue.main.async {
                self.mainThreadRealm = try! Realm()
            }
        }
        
        worker.executeBlock { [weak self] in
            self?.bgRealm = try? Realm()
        }
    }
    
    func get<DTO, DBO>(_ builder: DTObjectBuilder<DTO, DBO>.Type, id: Int?) -> DTO? {
        guard let id = id else { return nil }
        
        return fetch(builder,
                     filter: [NSPredicate(format: "id == %ld", id)],
                     sort: nil,
                     limit: 1).first
    }
    
    func fetch<DTO, DBO>(_ builder: DTObjectBuilder<DTO, DBO>.Type,
               filter: [NSPredicate] = [],
               sort: [SortParams]? = nil,
               limit: Int = 0) -> [DTO] {
        return getResult(builder, filter: filter, sort: sort, limit: limit, realm: try? Realm())
    }
    
    func fetchBG<DTO, DBO>(_ builder: DTObjectBuilder<DTO, DBO>.Type,
                         filter: [NSPredicate] = [],
                         sort: [SortParams]? = nil,
                         limit: Int = 0,
                         returnInMainThread: Bool = true,
                         completion: @escaping ([DTO]) -> Void) {
        worker.executeBlock { [weak self] in
            let result = self?.getResult(builder,
                                         filter: filter,
                                         sort: sort,
                                         limit: limit,
                                         realm: self?.bgRealm)
            if returnInMainThread {
                DispatchQueue.main.async { completion(result ?? []) }
            } else { completion(result ?? []) }
        }
    }
    
    func notifyObject<DTO, DBO>(
        _ builder: DTObjectBuilder<DTO, DBO>.Type,
        id: Int,
        triggers: [DBTrigger<DBO>] = [],
        createIfEmpty: Bool = false,
        block: @escaping (DBObjectChanges<DTO, DBO>) -> Void
    ) -> DBNotificationToken? {
        
        let dbObject: DBO
        
        if let object = DBO.get(id: id) {
            dbObject = object
        } else {
            if !createIfEmpty {
                return nil
            } else {
                let object = DBO()
                object.id = id
                let realm = mainThreadRealm!
                try? realm.safeWrite {
                    realm.add(object, update: true)
                }

                dbObject = object
            }
        }
        
        if let dto = builder.translate(dbObject: dbObject, realm: mainThreadRealm) {
            block(.initial(dto: dto))
        }

        let token = dbObject.observe { [weak self] change in
            switch change {
            case .change(let properties):
                guard
                    let realm = self?.mainThreadRealm,
                    let dbo = DBO.get(id: id),
                    let dto = builder.translate(dbObject: dbo, realm: realm)
                else {
                    Log.error("notifyChanges: Can't fetch object with id: \(id)")
                    return
                }
                
                if triggers.isEmpty {
                    block(.change(dto: dto, changes: [], rlmChanges: properties))
                } else {
                    let changes = triggers
                        .zip(properties) {
                            $0.stringRepresentaion == $1.name && $0.matchesAgainst(dbo)
                        }.filter {
                            !($0.0.equalPredicate($0.1.oldValue, $0.1.newValue))
                        }.map { DBPropertyChange(key: $0.0.keyPath,
                                                 oldValue: $0.1.oldValue,
                                                 newValue: $0.1.newValue )
                        }
                    if !changes.isEmpty {
                        block(.change(dto: dto, changes: changes, rlmChanges: properties))
                    }
                }
                
            case .error(let error): block(.error(error))
            case .deleted: block(.deleted)
            }
        }
        
        return DBNotificationToken(token: token)
    }
    
    func notify<DTO, DBO>(_ builder: DTObjectBuilder<DTO, DBO>.Type,
                         filter: [NSPredicate] = [],
                         sort: [SortParams]? = nil,
                         block: @escaping (DBChanges<DTO>) -> Void) -> DBNotificationToken? {
        guard Thread.isMainThread else {
            assertionFailure("Use in main thread only. Otherwise use notifyBG.")
            return nil
        }
        return createToken(builder,
                           filter: filter,
                           sort: sort,
                           realm: mainThreadRealm,
                           block: block)
    }
    
    func notifyBG<DTO, DBO>(_ builder: DTObjectBuilder<DTO, DBO>.Type,
                          filter: [NSPredicate] = [],
                          sort: [SortParams]? = nil,
                          tokenCreateBlock: @escaping (DBNotificationToken) -> Void,
                          updateBlock: @escaping (DBChanges<DTO>) -> Void) {
        worker.executeBlock { [weak self] in
            guard let strongSelf = self else { return }
            
            let token = strongSelf.createToken(builder,
                                       filter: filter,
                                       sort: sort,
                                       realm: strongSelf.bgRealm,
                                       block: updateBlock)
            tokenCreateBlock(token)
        }
        
    }
    
    func notifyCount<DBO: RealmModel>(_ type: DBO.Type,
                          filter: [NSPredicate] = [],
                          block: @escaping (DBCountEvent) -> Void) -> DBCounterToken? {
        guard Thread.isMainThread else {
            assertionFailure("Use in main thread only. Otherwise use notifyBG.")
            return nil
        }

        return createCounterToken(
            type,
            filter: filter,
            realm: mainThreadRealm,
            block: block
        )
    }
    
    private func createCounterToken<DBO: RealmModel>(
        _ type: DBO.Type,
        filter: [NSPredicate] = [],
        realm: Realm,
        block: @escaping (DBCountEvent) -> Void
    ) -> DBCounterToken {
        
        let result = realm.objects(DBO.self)
            .filter(predicates: filter)
            .filter("isEmpty == %@", NSNumber(value: false))
        
        let dbToken = DBCounterToken(initialValue: result.count)
        
        let wrappedBlock = { [weak dbToken] (changes: RealmCollectionChange<Results<DBO>>) -> Void in
            switch changes {
            case .initial(let result):
                let value = result.count
                dbToken?.value = value
                block(.initial(value: value))
                
            case .error(let error):
                block(.error(error))
                
            case .update(let result, _, _, _):
                let newValue = result.count
                if newValue != dbToken?.value {
                    dbToken?.value = newValue
                    block(.change(value: newValue))
                }
            }
        }
        
        dbToken.configure(token: result.observe(wrappedBlock))
        
        return dbToken
    }
    
    private func createToken<DTO, DBO>(
        _ builder: DTObjectBuilder<DTO, DBO>.Type,
        filter: [NSPredicate],
        sort: [SortParams]?,
        realm: Realm,
        block: @escaping (DBChanges<DTO>) -> Void
    ) -> DBNotificationToken {
        let token = realm.objects(DBO.self)
            .filter(predicates: filter)
            .filter("isEmpty == %@", NSNumber(value: false))
            .sorted(sort)
            .observe { (changes) in
                switch changes {
                case .initial(let result):
                    let dtos: [DTO] = result.map { $0 }
                        .compactMap { builder.translate(dbObject: $0, realm: result.realm!) }
                    block(.initial(result: dtos))
                    
                case .error(let error):
                    block(.error(error))
                    
                case .update(let result, let d, let i, let m):
                    let dtos: [DTO?] = Array(result)
                        .map { builder.translate(dbObject: $0, realm: result.realm!) }
                    block(.update(result: dtos, deletions: d, insertions: i, updates: m))
            }
        }
        return DBNotificationToken(token: token)
    }
    
    private func getResult<DTO, DBO>(
        _ builder: DTObjectBuilder<DTO, DBO>.Type,
        filter: [NSPredicate],
        sort: [SortParams]?,
        limit: Int,
        realm: Realm?) -> [DTO] {
        guard let realm = realm else { return [] }
        
        let result = realm.objects(DBO.self)
            .filter(predicates: filter)
            .filter("isEmpty == %@", NSNumber(value: false))
            .sorted(sort)
        if limit > 0, result.count > limit {
            return result[...(limit - 1)]
                .compactMap { builder.translate(dbObject: $0, realm: realm) }
        }
        
        return result.map { $0 }
            .compactMap { builder.translate(dbObject: $0, realm: realm) }
    }
}
