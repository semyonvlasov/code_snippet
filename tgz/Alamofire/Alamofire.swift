//
//  Alamofire.swift
//  togezzer-ios
//
//  Created by Семен Власов on 26.01.2018.
//  Copyright © 2018 tgz. All rights reserved.
//

import Alamofire
import JSONCodable

extension DataRequest {
    public func log() -> Self {
        return logRequest().logResponse()
    }
}

// MARK: - Request logging
extension DataRequest {
    func logRequest() -> Self {
        guard let method = request?.httpMethod,
            let path = request?.url?.absoluteString else {
            return self
        }
        
        Log.info("↗️ \(method) \(path)")
        if let data = request?.httpBody, let body = String(data: data, encoding: .utf8) {
            Log.verbose("\(method) \(path): \"\(body)\"")
        } else {
            Log.verbose("\(method) \(path)")
        }
        return self
    }
}

extension DataRequest {
    func logResponse() -> Self {
        return responseJSON { response in
            guard let code = response.response?.statusCode,
                let path = response.request?.url?.absoluteString else { return }
            
            Log.info("↘️ \(code) \(path)")
            if  let body = (DataRequest.getJson(response: response))?.json() {
                Log.verbose("\(code) \(path): \"\(body)\"")
            } else {
                Log.verbose("\(code) \(path)")
            }
        }
    }
    
    static private func getJson(response: DataResponse<Any>) -> JSONObject? {
        switch response.result {
        case .success(let v):
            return v as? JSONObject
        default:
            return nil
        }
    }
}

public extension Collection {
    func json() -> String {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self,
                                                      options: [.prettyPrinted])
            guard let jsonString = String(data: jsonData, encoding: String.Encoding.utf8) else {
                Log.error("Can't create string with data.")
                return "{}"
            }
            return jsonString
        } catch let parseError {
            Log.error("json serialization error: \(parseError)")
            return "{}"
        }
    }
}
