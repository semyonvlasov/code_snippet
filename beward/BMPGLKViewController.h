//
//  BMPGLKViewController.h
//  BWRTSPPlayer
//
//  Created by adm on 07.04.16.
//  Copyright © 2016 Beward Ltd. All rights reserved.
//

#import <GLKit/GLKit.h>

@class BMPVideoFrame;

@interface BMPGLKViewController : GLKViewController

- (void)playRenderingLoop;
- (void)pauseRenderingLoop;
- (void)stopRenderingLoop;

- (void)setFrame:(BMPVideoFrame*)frame;

@end
