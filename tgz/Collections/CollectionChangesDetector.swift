//
//  CollectionChanges.swift
//  togezzer-ios
//
//  Created by semyonvlasov on 18/01/2018.
//  Copyright © 2018 tgz. All rights reserved.
//

import Foundation

typealias CollectionData = (items: [CollectionModelProtocol], sections: [SectionModelProtocol])

protocol Uniquelyable {
    var uid: String { get }
}

struct CollectionChangeMove: Equatable, Hashable {
    let indexPathOld: IndexPath
    let indexPathNew: IndexPath

    var hashValue: Int {
        return (indexPathOld as NSIndexPath).hash ^ (indexPathNew as NSIndexPath).hash
    }
}

func == (lhs: CollectionChangeMove, rhs: CollectionChangeMove) -> Bool {
    return lhs.indexPathOld == rhs.indexPathOld && lhs.indexPathNew == rhs.indexPathNew
}

class CollectionChanges: NSObject {
    var insertedSections: IndexSet
    var removedSections: IndexSet
    
    var insertedIndexPaths: Set<IndexPath>
    var deletedIndexPaths: Set<IndexPath>
    var movedIndexPaths: [CollectionChangeMove]
    var itemsContentChanged: Bool
    
    init(insertedIndexPaths: Set<IndexPath>,
         deletedIndexPaths: Set<IndexPath>,
         movedIndexPaths: [CollectionChangeMove],
         insertedSections: IndexSet,
         removedSections: IndexSet,
         itemsContentChanged: Bool) {
        
        self.insertedIndexPaths = insertedIndexPaths
        self.deletedIndexPaths = deletedIndexPaths
        self.movedIndexPaths = movedIndexPaths
        
        self.insertedSections = insertedSections
        self.removedSections = removedSections
        
        self.itemsContentChanged = itemsContentChanged
        
        super.init()
    }
}

class CollectionChangesDetector {
    
    class func generateIndexPath(index: Int?, sections: [Int]) -> IndexPath {
        var section = 0
        var item = index ?? 0
        for count in sections {
            if item >= count {
                section += 1
                item -= count
            } else { break }
        }
        
        return IndexPath(item: item, section: section)
    }
    
    class func generateChanges(oldCollectionData: CollectionData,
                               newCollectionData: CollectionData) -> CollectionChanges {
        func generateIndexesById(_ uids: [String]) -> [String: Int] {
            var map = [String: Int](minimumCapacity: uids.count)
            for (index, uid) in uids.enumerated() {
                map[uid] = index
            }
            return map
        }
        
        let oldCollection = oldCollectionData.items
        let newCollection = newCollectionData.items
        let oldSections = oldCollectionData.sections.map { $0.items }
        let newSections = newCollectionData.sections.map { $0.items }
        
        let oldSectionIndexes = oldCollectionData.sections.map { $0.type }
        let newSectionIndexes = newCollectionData.sections.map { $0.type }
        
        let oldIds = oldCollection.map { $0.uid }
        let newIds = newCollection.map { $0.uid }
        let oldIndexsById = generateIndexesById(oldIds)
        let newIndexsById = generateIndexesById(newIds)
        var deletedIndexPaths = Set<IndexPath>()
        var insertedIndexPaths = Set<IndexPath>()
        var movedIndexPaths = [CollectionChangeMove]()
        
        var itemsContentChanged = false
        
        var removedSections = IndexSet()
        oldSectionIndexes.enumerated().forEach {
            if !newSectionIndexes.contains($0.element) { removedSections.insert($0.offset) }
        }
        
        var insertedSections = IndexSet()
        newSectionIndexes.enumerated().forEach {
            if !oldSectionIndexes.contains($0.element) { insertedSections.insert($0.offset) }
        }
        
        // Deletetions
        for oldId in oldIds {
            let isDeleted = newIndexsById[oldId] == nil
            if isDeleted {
                let indexPath = generateIndexPath(index: oldIndexsById[oldId] ?? 0,
                                                  sections: oldSections)
                if !removedSections.contains(indexPath.section) {
                    deletedIndexPaths.insert(indexPath)
                }
            }
        }
        
        // Insertions and movements
        for newId in newIds {
            let newIndex = newIndexsById[newId] ?? 0
            let newIndexPath = generateIndexPath(index: newIndex, sections: newSections)
            if let oldIndex = oldIndexsById[newId] {
                if !itemsContentChanged,
                    newCollection[safe: newIndex]?.contentIdentityUid !=
                        oldCollection[safe: oldIndex]?.contentIdentityUid {
                    itemsContentChanged = true
                }
                if oldIndex != newIndex {
                    let oldIndexPath = generateIndexPath(index: oldIndex, sections: oldSections)
                    movedIndexPaths.append(CollectionChangeMove(indexPathOld: oldIndexPath,
                                                                indexPathNew: newIndexPath))
                }
            } else {
                // It's new
                insertedIndexPaths.insert(newIndexPath)
            }
        }
        
        return CollectionChanges(insertedIndexPaths: insertedIndexPaths,
                                 deletedIndexPaths: deletedIndexPaths,
                                 movedIndexPaths: movedIndexPaths,
                                 insertedSections: insertedSections,
                                 removedSections: removedSections,
                                 itemsContentChanged: itemsContentChanged)
    }
    
    class func updated<T: Any>(collection: [IndexPath: T],
                               withChanges changes: CollectionChanges) -> [IndexPath: T] {
        var result = collection.filter { !changes.removedSections.contains($0.key.section) }
        
        changes.deletedIndexPaths.forEach { (indexPath) in
            result[indexPath] = nil
        }
        
        var movedDestinations = Set<IndexPath>()
        changes.movedIndexPaths.forEach { (move) in
            result[move.indexPathNew] = collection[move.indexPathOld]
            movedDestinations.insert(move.indexPathNew)
            if !movedDestinations.contains(move.indexPathOld) {
                result[move.indexPathOld] = nil
            }
        }
        
        changes.insertedIndexPaths.forEach { (indexPath) in
            result[indexPath] = nil
        }
        
        return result
    }
}
