//
//  DBNotificationToken.swift
//  togezzer-ios
//
//  Created by Семен Власов on 14.09.2018.
//  Copyright © 2018 tgz. All rights reserved.
//

import Foundation
import RealmSwift

class DBNotificationToken {
    private var token: NotificationToken
    
    init(token: NotificationToken) {
        self.token = token
    }
    
    func stop() {
        token.invalidate()
    }
}

class DBCounterToken {
    
    private var token: NotificationToken?
    var value: Int
    
    init(initialValue value: Int) {
        self.value = value
    }
    
    func stop() {
        token?.invalidate()
    }
    
    //Internal DBStorage use only
    func configure(token: NotificationToken) {
        guard self.token == nil else { return }
        self.token = token
    }
}
