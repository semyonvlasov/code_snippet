//
//  BackgroundWorker.swift
//  togezzer-ios
//
//  Created by Семен Власов on 12.09.2018.
//  Copyright © 2018 tgz. All rights reserved.
//

import Foundation

class BackgroundWorker: NSObject {
    var thread: Thread!
    private var executeQueue = DispatchQueue(label: "bgWorker_queue-\(UUID().uuidString)")
    
    func executeBlock(_ block: @escaping () -> Void) {
        executeQueue.async { [weak self] in
            guard let strongSelf = self else { return }
            let selector = NSSelectorFromString("runBlock:")
            
            strongSelf.perform(
                selector,
                on: strongSelf.thread,
                with: block,
                waitUntilDone: false,
                modes: [RunLoop.Mode.default.rawValue])
        }
    }
    
    func start() {
        let threadName = String(describing: self)
            .components(separatedBy: .punctuationCharacters)[1]
        
        thread = Thread(target: self, selector: #selector(runLoop), object: nil)
        thread.name = "\(threadName)-\(UUID().uuidString)"
        thread.start()
    }
    
    @objc private func runLoop() {
        while !self.thread.isCancelled {
            RunLoop.current.run(
                mode: RunLoop.Mode.default,
                before: Date.distantFuture)
        }
        Thread.exit()
    }
    
    @objc private func runBlock(_ block: Any?) {
        if let block = block as? (() -> Void) {
            block()
        }
    }
    
    public func stop() {
        thread.cancel()
    }
}
