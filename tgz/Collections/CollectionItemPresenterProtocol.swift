//
//  CollectionCellPresenter.swift
//  togezzer-ios
//
//  Created by semyonvlasov on 18/01/2018.
//  Copyright © 2018 tgz. All rights reserved.
//

import UIKit
import SwipeCellKit

protocol CollectionItemPresenterProtocol: class {
    
    var canCalculateHeightInBackground: Bool { get }
    var cellTopMargin: CGFloat { get }
    
    static func registerCells(_ collection: CollectionTypeViewProtocol)
    func dequeueCell(_ collection: CollectionTypeViewProtocol,
                     indexPath: IndexPath) -> CollectionTypeViewCellProtocol
    func configureCell(_ cell: CollectionTypeViewCellProtocol,
                       animated: Bool,
                       additionalConfiguration: (() -> Void)?)
    
    func heightForCell(maximumWidth width: CGFloat?) -> CGFloat?
    
    func shouldShowMenu() -> Bool // optional. Default is false
    func canPerformMenuControllerAction(_ action: Selector) -> Bool // optional. Default is false
    func performMenuControllerAction(_ action: Selector) // optional
    
    func cellDidPress()
    
    func cellWillBeShown(_ cell: CollectionTypeViewCellProtocol) // optional
    func cellWasHidden(_ cell: CollectionTypeViewCellProtocol) // optional
    
    func editActions(for orientation: SwipeActionsOrientation) -> [SwipeAction]? // optional
    func editActionsOptions(for orientation: SwipeActionsOrientation) -> SwipeOptions // optional
}

extension CollectionItemPresenterProtocol {
    
    var canCalculateHeightInBackground: Bool { return false }
    var cellTopMargin: CGFloat { return 8 }
    
    func heightForCell(maximumWidth width: CGFloat?) -> CGFloat? { return nil }
    func shouldShowMenu() -> Bool { return false }
    func canPerformMenuControllerAction(_ action: Selector) -> Bool { return false }
    func performMenuControllerAction(_ action: Selector) {}
    
    func cellDidPress() { }
    
    func cellWillBeShown(_ cell: CollectionTypeViewCellProtocol) { }
    func cellWasHidden(_ cell: CollectionTypeViewCellProtocol) { }
    
    func editActions(for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        return nil
    }
    
    func editActionsOptions(for orientation: SwipeActionsOrientation) -> SwipeOptions {
        return SwipeOptions()
    }
}
