//
//  CollectionDataSourceProtocol.swift
//  togezzer-ios
//
//  Created by semyonvlasov on 18/01/2018.
//  Copyright © 2018 tgz. All rights reserved.
//

import Foundation

@objc enum CollectionUpdateType: Int {
    case firstLoad //Первоначальная загрузка
    case loadNew //Подгрузка новых элементов сверху
    case pagination //Подгрузка старых элементов снизу
    case reload //Полная перезагрузка
    case update //Обновление конкретных элементов
}

typealias CollectionUpdateData = (items: [CollectionModelProtocol], sections: [SectionModelProtocol])
protocol CollectionDataSourceDelegateProtocol: class {
    func dataSourceDidUpdate(updateData: CollectionUpdateData,
                             updateType: CollectionUpdateType,
                             completion:  (() -> Void)?)
}

protocol CollectionDataSourceProtocol: class {
    
    var delegate: CollectionDataSourceDelegateProtocol? { get set }
    var needReloadVisibleCells: Bool { get }
    var hasMorePrevious: Bool { get }
    
    func reload(indexPaths: [IndexPath], completion: (() -> Void)?)
    func reload()
    func loadNew()
    func loadPrevious()
    
    func remove(model: CollectionModelProtocol)
}

extension CollectionDataSourceProtocol {
    var hasMorePrevious: Bool { return false }
    var needReloadVisibleCells: Bool { return false }
    
    func reload(indexPaths: [IndexPath], completion: (() -> Void)?) {}
    func loadNew() {}
    func loadPrevious() {}
    func remove(model: CollectionModelProtocol) {}
}
