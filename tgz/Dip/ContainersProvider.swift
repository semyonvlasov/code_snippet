//
//  BaseContainer.swift
//  togezzer-ios
//
//  Created by semyonvlasov on 18/01/2018.
//  Copyright © 2018 tgz. All rights reserved.
//

import Foundation
import Dip

class ContainersProvider {
    static let shared = ContainersProvider()
    
    private var containers = [String: Weak<DependencyContainer>]()
    private var transactionsCount = 0
    
    func addContainer(_ container: DependencyContainer) {
        addContainer(container, tag: "tag_\(transactionsCount)")
    }
    
    func addContainer(_ container: DependencyContainer, tag: String) {
        containers[tag] = Weak(value: container)
        transactionsCount += 1
    }
    
    func removeContainer(tag: String) {
        containers[tag] = nil
    }

    func getContainers() -> [DependencyContainer] {
        return containers.map { $0.1.value }.compactMap { $0 }
    }
    
    func removeWeaks() {
        for (key, value) in containers.map({ ($0.0, $0.1) }) {
            if value.value == nil {
                containers[key] = nil
            }
        }
    }
}
