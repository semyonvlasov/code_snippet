//
//  BWDUtils.h
//  camDrive
//
//  Created by adm on 27.11.15.
//  Copyright © 2015 adm All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BWDUtils : NSObject

//+ (NSUInteger)getLineNumberForLabel:(UILabel *)label;
+ (void)shakeAnimation:(NSArray*)viewArray;
+ (void)borderColorChangeAnimation:(NSArray*)viewArray toColor:(UIColor *)color;
+ (NSDate *)dateOnlyFromDate:(NSDate *)date;
+ (NSDate *)yesterdayFromDate:(NSDate *)date;
+ (NSDate *)tomorrowFromDate:(NSDate *)date;
+ (NSDate *)weekStartFromDate:(NSDate *)date;
+ (BOOL)isSaturdayDate:(NSDate *)date;
+ (BOOL)isSundayDate:(NSDate *)date;
+ (CGFloat)getLineHeightForFont:(UIFont *)font;
+ (NSString *)genRandomIdWithLength:(unsigned)length;
+ (NSString *)getUserAgentString;
@end
