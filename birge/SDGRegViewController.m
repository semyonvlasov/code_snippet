//
//  SDGRegViewController.m
//  Birge
//
//  Created by Семен Власов on 27.08.15.
//  Copyright (c) 2015 SVV. All rights reserved.
//

#import "SDGRegViewController.h"
#import "MBProgressHUD.h"
#import "SDGUtils.h"
#import "APIManager.h"

#warning нужна ссылка на политику конфиденциальности

@interface SDGRegViewController () <UITextFieldDelegate, UIAlertViewDelegate>

@property (nonatomic, weak) IBOutlet UITextField *loginTF;
@property (nonatomic, weak) IBOutlet UITextField *passTF;
@property (nonatomic, weak) IBOutlet UITextField *passConfirmTF;
@property (nonatomic, weak) IBOutlet UISwitch *switchControl;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *topConstraint;

@end

@implementation SDGRegViewController {
    UITapGestureRecognizer *_tapGesture;
    UITextField *_activeTextField;
    CGFloat _curKeyboardHeight;
    CGFloat _initialTopOffset;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognize:)];
    _tapGesture.enabled = NO;
    [self.view addGestureRecognizer:_tapGesture];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    _initialTopOffset = _topConstraint.constant;
}

- (void)tapGestureRecognize:(UITapGestureRecognizer *)recognizer {
    if (_activeTextField != nil) [_activeTextField resignFirstResponder];
}

- (IBAction)regButtTouchUp:(id)sender {
    if (_loginTF.text.length > 0 && _passTF.text.length > 0 && _passConfirmTF.text.length > 0) {
        if (![_passTF.text isEqualToString:_passConfirmTF.text]) {
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Подтверждение пароля не верно" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            av.tag = 1;
            [av show];
        }
        else if (![_switchControl isOn]) {
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"" message:@"Необходимо подтвердить, что Вы согласны с условиями пользовательского соглашения" delegate:self cancelButtonTitle:@"Подтвердить" otherButtonTitles:nil];
            av.tag = 2;
            [av show];
        }
        else {
            [self tryReg];
        }
    }
    else {
        NSMutableArray *tmpArr = [[NSMutableArray alloc] initWithCapacity:3];
        if (_loginTF.text.length == 0) [tmpArr addObject:_loginTF];
        if (_passTF.text.length == 0) [tmpArr addObject:_passTF];
        if (_passConfirmTF.text.length == 0) [tmpArr addObject:_passConfirmTF];
        
        [SDGUtils borderColorChangeAnimation:[NSArray arrayWithArray:tmpArr] toColor:nil];
    }
}

- (void)tryReg {
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.dimBackground = YES;
    [self.navigationController.visibleViewController.view addSubview:HUD];
    [HUD show:YES];
    
    [[APIManager sharedInstance] registerWithLogin:_loginTF.text pass:_passTF.text completion:^(id result) {
        [HUD hide:YES];
        
        if ([result isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dict = result;
            if (dict[@"errors"]) {
                UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:dict[@"errors"][0] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [av show];
            }
            else {
                UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"" message:@"Для активации учетной записи, Вам на почту, отправлена ссылка, по которой необходимо пройти" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                av.tag = 3;
                [av show];
            }
        }
    } failure:^(NSError *error) {
        [HUD hide:YES];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Не удалось зарегистрироваться" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [av show];
    }];
}

#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        _passConfirmTF.text = @"";
        [_passConfirmTF becomeFirstResponder];
    }
    else if (alertView.tag == 2) {
        [_switchControl setOn:YES animated:YES];
    }
    else if (alertView.tag == 3) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark UITextFieldDelegate and keyboard handlers

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    _tapGesture.enabled = YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    _tapGesture.enabled = NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    _activeTextField = textField;
    
    if (_curKeyboardHeight > 0) {
        [self animateTextFieldOffset];
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField != _passConfirmTF) {
        if (textField == _loginTF) {
            [_passTF becomeFirstResponder];
        }
        else {
            [_passConfirmTF becomeFirstResponder];
        }
    }
    else {
        [_passConfirmTF resignFirstResponder];
    }
    return YES;
}

- (void)keyboardWillShow:(NSNotification *)notif {
    CGSize kbSize = [[[notif userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    _curKeyboardHeight = kbSize.height;
    if (_activeTextField) {
        [self animateTextFieldOffset];
    }
}

- (void)keyboardWillHide:(NSNotification *)notif {
    _curKeyboardHeight = 0;
    
    if (_activeTextField && _topConstraint.constant != _initialTopOffset) {
        _topConstraint.constant = _initialTopOffset;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

- (void)animateTextFieldOffset {
    if (_activeTextField) {
        CGFloat offset = (_activeTextField.frame.origin.y + _activeTextField.frame.size.height + 10) - (self.view.frame.size.height - _curKeyboardHeight);
        if (offset > 0) {
            _topConstraint.constant = _topConstraint.constant - offset;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            }];
        }
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
