//
//  NSUserDefaults+StaticType.swift
//  togezzer-ios
//
//  Created by Semyon Vlasov on 26.08.16.
//
//

import Foundation

protocol IStorage: class {
    var userId: Int? { get }
    
    func save<T: Codable>(_ value: T?, forKey key: DefaultsKey<T>)
    func load<T: Codable>(key: DefaultsKey<T>) -> T?
    
    func save<T: Codable>(_ value: T?, forKey key: PersonalKey<T>)
    func load<T: Codable>(key: PersonalKey<T>) -> T?
}

protocol ISecureStorage: class {
    func save<T: Codable>(_ value: T?, forKey key: SecureKey<T>)
    func load<T: Codable>(key: SecureKey<T>) -> T?
}

let Defaults = TGZDefaults(suiteName: "TzzStaticTypeDefaultsStorage")!

class TGZDefaults: UserDefaults {
    weak var userIdProvider: IAuthChecker?
    private lazy var keychainService: KeychainService = {
        return KeychainService(service: serviceName, accessGroup: nil)
    }()
}

class DefaultsKeys {
    fileprivate init() {}
}

class DefaultsKey<ValueType: Codable>: DefaultsKeys {
    let key: String
    
    init(_ key: String) {
        self.key = key
        super.init()
    }
}

class PersonalKey<ValueType: Codable>: DefaultsKey<ValueType> {}
class SecureKey<ValueType: Codable>: DefaultsKey<ValueType> {}

struct CodableWrapper<T: Codable>: Codable {
    let val: T
}

extension TGZDefaults: IStorage, ISecureStorage {
    
    var userId: Int? { return userIdProvider?.currentUserId() }
    
    //MARK: - Save
    func save<T: Codable>(_ value: T?, forKey key: DefaultsKey<T>) {
        guard let value = value else {
            removeObject(forKey: key.key)
            return
        }
        if let jsonString = makeJSONString(from: value) {
            set(jsonString, forKey: key.key)
        }
    }
    
    //MARK: - Load
    func load<T: Codable>(key: DefaultsKey<T>) -> T? {
        let value: T?
        if let string = Defaults.value(forKey: key.key) as? String {
            value = makeObject(fromJSONString: string)
        } else {
            value = nil
        }
        return value
    }
    
    //MARK: - Personal settings
    func save<T: Codable>(_ value: T?, forKey key: PersonalKey<T>) {
        if let defaultsKey = getDefaultsKey(key: key, userId: userId) {
            save(value, forKey: defaultsKey)
        }
    }
    
    func load<T: Codable>(key: PersonalKey<T>) -> T? {
        guard let defaultsKey = getDefaultsKey(key: key, userId: userId) else { return nil }
        return load(key: defaultsKey)
    }
    
    //MARK: - Keychain settings
    #if DEV || LOCAL
    var serviceName: String { return "tzz_keychain_service_dev" }
    var groupName: String { return "tzz_keychain_sharing_dev" }
    #elseif RC
    var serviceName: String { return "tzz_keychain_service_rc" }
    var groupName: String { return "tzz_keychain_sharing_rc" }
    #elseif MASTER
    var serviceName: String { return "tzz_keychain_service_prod" }
    var groupName: String { return "tzz_keychain_sharing_prod" }
    #endif

    func save<T: Codable>(_ value: T?, forKey key: SecureKey<T>) {
        guard let value = value else {
            keychainService.remove(for: key.key)
            return
        }
        if let jsonString = makeJSONString(from: value) {
            keychainService.save(jsonString, for: key.key)
        }
    }
    
    func load<T: Codable>(key: SecureKey<T>) -> T? {
        let value: T?
        if let string = keychainService.get(for: key.key) {
            value = makeObject(fromJSONString: string)
        } else {
            value = nil
        }
        return value
    }
    
    //MARK: - Private functions
    private func getDefaultsKey<T: Any>(key: PersonalKey<T>,
                                        userId: Int?) -> DefaultsKey<T>? {
        assert(userId != nil, "TGZDefaults Access to personal settings before userId is set")
        
        guard let userId = userId else { return nil }
        
        let stringKey = key.key + "_user_\(userId)"
        return DefaultsKey<T>(stringKey)
    }
    
    private func makeJSONString<T: Codable>(from value: T) -> String? {
        let wrapper = CodableWrapper(val: value)
        
        guard
            let encodedData = try? JSONEncoder().encode(wrapper),
            let string = String(data: encodedData, encoding: String.Encoding.utf8)
        else {
            Log.error("TGZDefaults can't create JSON string from: \(value)")
            return nil
        }
        
        return string
    }
    
    private func makeObject<T: Codable>(fromJSONString string: String) -> T? {
        guard
            let data = string.data(using: String.Encoding.utf8),
            let wrapper = try? JSONDecoder().decode(CodableWrapper<T>.self, from: data)
        else {
            Log.error("TGZDefaults can't create object from JSON string:: \(string)")
            return nil
        }
        
        return wrapper.val
    }
}
