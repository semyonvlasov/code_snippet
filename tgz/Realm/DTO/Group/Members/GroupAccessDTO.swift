//
//  GroupAccessDTO.swift
//  togezzer-ios
//
//  Created by Семен Власов on 04/10/2019.
//  Copyright © 2019 tgz. All rights reserved.
//

struct GroupAccessDTO: GroupAccessDTOProtocol {
    let shortGroupDTO: ShortGroupDTOProtocol
    let members: [GroupMemberDTO]
}

protocol GroupAccessDTOProtocol: DecoratedShortGroupDTOProtocol {
    var members: [GroupMemberDTO] { get }
}

extension GroupAccessDTO {
    var activeMembersCount: Int {
        return members.filter { $0.groupStatus == .active }.count
    }
    
    var activeUserIds: [Int] {
        return members.filter { $0.groupStatus == .active }.map { $0.user.id }
    }
}
