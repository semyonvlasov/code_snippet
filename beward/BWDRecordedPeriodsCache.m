//
//  BWDRecordedPeriodsCache.m
//  camDrive
//
//  Created by adm on 26.02.16.
//  Copyright © 2016 adm. All rights reserved.
//

#import "BWDRecordedPeriodsCache.h"
#import "BWDRecordedPeriodsCacheItem.h"
#import "BWDUtils.h"
#import "NSDate+Utilities.h"
#import "BWDApiManager.h"

@implementation BWDRecordedPeriodsCache {
    NSMutableDictionary *_dataCache;
    NSMutableDictionary *_completionBlocks;
    
    NSDate *_minDate;
    NSDate *_maxDate;
}


+ (id)sharedManager {
    static BWDRecordedPeriodsCache *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


- (id)init {
    self = [super init];
    if (self) {
        _maxDate = [[NSDate date] dateByAddingDays:1];
        _minDate = [_maxDate dateBySubtractingDays:archiveLivePeriod];
        
        _dataCache = [[NSMutableDictionary alloc] init];
        _completionBlocks = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)clearCache {
    _dataCache = [[NSMutableDictionary alloc] init];
    _completionBlocks = [[NSMutableDictionary alloc] init];
}

- (BOOL)existsDataForChannelId:(NSString *)channelId date:(NSDate *)date direction:(int)direction {
    BWDRecordedPeriodsCacheItem *ret = [self getDataForChannelId:channelId date:date direction:direction];
    return (ret != nil) && (ret.state != BWDTimelineDataStateLoading);
}

- (BWDRecordedPeriodsCacheItem *)getDataForChannelId:(NSString *)channelId date:(NSDate *)date direction:(int)direction {
    BWDRecordedPeriodsCacheItem *ret = nil;
    if (direction == 0) {
        ret = [_dataCache safeObjectForKey:[self keyStringWithId:channelId date:date]];
    }
    else {
        NSDate *nextDate = [date copy];
        do {
            nextDate = (direction < 0) ? [BWDUtils yesterdayFromDate:nextDate] : [BWDUtils tomorrowFromDate:nextDate];
            ret = [_dataCache safeObjectForKey:[self keyStringWithId:channelId date:nextDate]];
        } while (ret != nil || ret.state == BWDTimelineDataStateNoData);
        
    }
    return ret;
}

- (void)loadDataForChannelId:(NSString *)channelId date:(NSDate *)date direction:(int)direction completion:(void(^)(BWDRecordedPeriodsCacheItem *result))completion {
    
    __block NSString *bChannelId = [channelId copy];
    __block NSDate *bDate = [date copy];
    __block NSDate *bNextDate = [date copy];
    __block int bDirection = direction;
    
    BWDRecordedPeriodsCacheItem *ret = [self getDataForChannelId:channelId date:date direction:direction];
    
    if (ret) {
        if (ret.state != BWDTimelineDataStateLoading) {
            if (completion) completion(ret);
            [self executeCompletionBlocksForItemKey:[self keyStringWithId:bChannelId date:ret.date] withItem:ret];
        }
        else {
            [self saveCompletionBlock:completion forItemKey:[self keyStringWithId:bChannelId date:ret.date]];
        }
    }
    else {
        ret = [[BWDRecordedPeriodsCacheItem alloc] initWithDate:((bDirection != 0) ? bNextDate : bDate) dataState:BWDTimelineDataStateLoading recordedPeriods:nil channelId:bChannelId];
        [_dataCache setObject:ret forKey:[self keyStringWithId:bChannelId date:ret.date]];
        
        [[BWDApiManager sharedInstance] getRecordedIntervalsWithDate:bDate channelId:bChannelId direction:bDirection motion:nil completion:^(id result) {
            NSDateFormatter *formatter= [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"YYYY-MM-dd"];
            NSString *dateString = [result safeObjectForKey:@"date"];
            NSDate *itemDate = [formatter dateFromString:dateString];
            
            if (!itemDate && bDirection != 0) {
                bNextDate = [bNextDate copy];
                do {
                    [_dataCache setObject:[[BWDRecordedPeriodsCacheItem alloc] initWithDate:[bNextDate copy] dataState:BWDTimelineDataStateNoData recordedPeriods:nil channelId:bChannelId]
                                           forKey:[self keyStringWithId:channelId date:bNextDate]];
                    bNextDate = (bDirection > 0) ? [BWDUtils tomorrowFromDate:bNextDate] : [BWDUtils yesterdayFromDate:bNextDate];
                } while (([itemDate timeIntervalSince1970] * bDirection) < ([((bDirection > 0) ? _maxDate : _minDate) timeIntervalSince1970] * bDirection));
                return;
            }
            
            //BWDRecordedPeriodsCacheItem *item = [_dataCache safeObjectForKey:[self keyStringWithId:channelId date:itemDate]];
            //if (!item) {
            //    return;
            //}
            
            BWDRecordedPeriodsCacheItem *item = [[BWDRecordedPeriodsCacheItem alloc] initWithDate:itemDate dataState:BWDTimelineDataStateNoData recordedPeriods:nil channelId:bChannelId];
            NSArray *intervals = (NSArray *)[result safeObjectForKey:@"intervals"];
            NSMutableArray *periods;
            if (intervals && [intervals count] > 0) {
                periods = [[NSMutableArray alloc] init];
                for (NSDictionary *dict in intervals) {
                    NSTimeInterval start = [(NSNumber *)[dict safeObjectForKey:@"start"] floatValue];
                    NSTimeInterval end = [(NSNumber *)[dict safeObjectForKey:@"end"] floatValue];
                    [periods addObject:[[BWDDatePeriod alloc] initWithStartInterval:start endInterval:end]];
                }
                item.state = BWDTimelineDataStateExists;
                item.periods = [NSArray arrayWithArray:periods];
                
            }
            [_dataCache setObject:item forKey:[self keyStringWithId:channelId date:itemDate]];
            if (completion) completion(item);
            [self executeCompletionBlocksForItemKey:[self keyStringWithId:bChannelId date:item.date] withItem:item];
            
        } failure:^(NSError *error) {
            NSString *key = [self keyStringWithId:bChannelId date:(bDirection == 0) ? bDate : bNextDate];
            BWDRecordedPeriodsCacheItem *item = [_dataCache safeObjectForKey:key];
            if (item && bChannelId) {
                [_dataCache removeObjectForKey:key];
                [_completionBlocks removeObjectForKey:key];
            }
        }];
    }

}

- (NSString *)keyStringWithId:(NSString *)cId date:(NSDate *)date {
    return [NSString stringWithFormat:@"%@%@", cId, [date serverDateString]];
}

- (void)saveCompletionBlock:(void(^)(BWDRecordedPeriodsCacheItem *result))completion forItemKey:(NSString *)itemKey {
    if (!completion) {
        return;
    }
    NSMutableArray *blocksArray = [_completionBlocks safeObjectForKey:itemKey];
    if (blocksArray) {
        [blocksArray addObject:completion];
    }
    else {
        [_completionBlocks setObject:[NSMutableArray arrayWithObject:completion] forKey:itemKey];
    }
}

- (void)executeCompletionBlocksForItemKey:(NSString *)itemKey withItem:(BWDRecordedPeriodsCacheItem *)item{
    NSMutableArray *blocksArray = [_completionBlocks safeObjectForKey:itemKey];
    if (blocksArray) {
        [_completionBlocks removeObjectForKey:itemKey];
        for (void(^ completion)(BWDRecordedPeriodsCacheItem *result) in blocksArray) {
            completion(item);
        }
    }
}

@end
