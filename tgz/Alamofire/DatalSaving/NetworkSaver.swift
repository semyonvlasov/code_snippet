//
//  NetworkSaver.swift
//  togezzer-ios
//
//  Created by semyonvlasov on 26/07/2017.
//  Copyright © 2017 tgz. All rights reserved.
//

import Foundation
import JSONCodable
import RealmSwift

final class NetworkSaver<Processor: NetworkModelProcessor> {
    
    typealias Model = Processor.Model
    typealias DBO = Processor.DBO
    typealias Builder = Processor.Builder
    typealias Saver = Processor.Saver
    
    @discardableResult
    static func save(data: [JSONObject], realm: Realm? = nil) -> [DBO] {
        let models = data.compactMap { (data) -> Model? in
            guard let model = Builder.createModel(from: data) else {
                Log.error(
                    "Deserialization for \(Builder.Model.self)" +
                        " failed with data: \(data)"
                )
                return nil
            }
            
            return model
        }
        
        return DBSaver<Saver>.save(models: models, realm: realm)
    }
    
    @discardableResult
    static func save(data: JSONObject, realm: Realm? = nil) -> DBO? {
        return save(data: [data]).first
    }
    
    ///Для обновления созданных на девайсе моделек (например, новых сообщений)
    @discardableResult
    static func forceMerge(data: JSONObject, toId id: Int) -> Int? {
        guard let model = Builder.createModel(from: data) else { return nil }
        
        return DBSaver<Saver>.forceMerge(model: model, toId: id)
    }
}
