//
//  ShortGroupDTOBuilder.swift
//  togezzer-ios
//
//  Created by Семен Власов on 04/10/2019.
//  Copyright © 2019 tgz. All rights reserved.
//

import RealmSwift

class ShortGroupDTOBuilder: DTObjectBuilder<ShortGroupDTO, Group> {
    
    override class func translateNonEmpty(dbObject: Group, realm: Realm) -> ShortGroupDTO? {
        guard
            let baseDTO = getBaseDTO(dbObject: dbObject),
            let type = GroupType(rawValue: dbObject.type)
        else { return nil }
        
        let avatar = type == .company && dbObject.avatar == nil
            ? AvatarManager.getAvatarData(model: dbObject.company)
            : AvatarManager.getAvatarData(model: dbObject)

        return ShortGroupDTO(
            baseDTO: baseDTO,
            type: type,
            title: dbObject.title,
            avatar: avatar
        )
    }
}
