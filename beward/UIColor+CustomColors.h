//
//  UIColor+CustomColors.h
//  camDrive
//
//  Created by adm on 16.05.16.
//  Copyright © 2016 adm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CustomColors)

+ (UIColor *)BWDGrayColor;
//+ (UIColor *)BWDDarkGrayColor;
+ (UIColor *)BWDLightGrayColor;

+ (UIColor *)BWDDarkGrayFontColor;
+ (UIColor *)BWDLightGrayFontColor;
+ (UIColor *)BWDGrayFontColor;

+ (UIColor *)BWDBlueColor;
+ (UIColor *)BWDRedColor;
+ (UIColor *)BWDGreenColor;
+ (UIColor *)BWDOrangeColor;

@end
