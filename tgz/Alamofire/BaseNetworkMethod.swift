//
//  BaseNetworkMethod.swift
//  togezzer-ios
//
//  Created by semyonvlasov on 16.10.2017.
//  Copyright © 2017 tgz. All rights reserved.
//

import Foundation
import Alamofire
import JSONCodable

typealias NetworkMethodError = (Any?) -> Void

class BaseNetworkMethod {
    static var baseUrl: String { return PreferencesHelper.getBackendUrl() + "/api/" }
    
    var method: HTTPMethod { return HTTPMethod.get }
    var encoding: URLEncoding { return URLEncoding.default }
    let authChecker: IAuthChecker = try! BaseContainer().resolve()
    let authControl: IAuthControl = try! BaseContainer().resolve()
    
    func getUrl() -> String {
        return BaseNetworkMethod.baseUrl
    }
    
    func getHeaders() -> [String: String] {
        return [
            "Authorization": "Basic \(PreferencesHelper.basicAuthToken)",
            "User-Agent": PreferencesHelper.getClientInfo()
        ]
    }
    
    func getParams() -> [String: String] {
        return [:]
    }
    
    func getPayload() -> [String: Any] {
        return [:]
    }
    
    func checkResponse(response: DataResponse<Any>) -> JSONObject? {
        let url = response.request?.url?.absoluteString ?? "unknown"
        switch response.result {
        case .success(let value):
            if let json = value as? JSONObject {
                return json
            } else {
                printError(url: url, details: "response cast error")
            }
            
        case .failure(let error):
            printError(url: url, details: error)
        }
        
        return nil
    }
    
    func printError(response: DataResponse<Any>, details: Any) {
        let url = response.request?.url?.absoluteString ?? "unknown"
        self.printError(url: url, details: details)
    }
    
    private func printError(url: String, details: Any) {
        Log.error(" for \(url) details: \(details)")
    }
    
    private func buildUrl() -> URL {
        var queryItems = [URLQueryItem]()
        for (name, value) in self.getParams() {
            queryItems.append(URLQueryItem(name: name, value: value))
        }
        var urlComps = URLComponents(string: self.getUrl())!
        urlComps.queryItems = queryItems as [URLQueryItem]
        return urlComps.url!
    }
    
    func dataExtractor(json: JSONObject) -> Any {
        guard let data = json["data"] else {
            return [:]
        }
        return data
    }
    
    func gettingData(json: JSONObject) -> Any? {
        return nil
    }
    
    typealias NetworkMethodSuccess = (JSONObject, Any?) -> Void
    
    func perform(queue: DispatchQueue? = nil,
                 success: NetworkMethodSuccess? = nil,
                 error: NetworkMethodError? = nil) {
        let statistics: IStatisticsClient = try! BaseContainer().resolve()
        let from = Date().timeIntervalSince1970
        
        Alamofire.request(self.buildUrl(),
                          method: self.method,
                          parameters: self.method == .get ? nil : self.getPayload(),
                          encoding: self.method != .delete ? self.encoding : URLEncoding.httpBody,
                          headers: self.getHeaders())
            .validate()
            .log()
            .responseJSON(queue: queue) { response in
                var isError = false
                //For delete methods with 204 response
                if let statusCode = response.response?.statusCode, statusCode == 204 {
                    success?(JSONObject(), nil)
                } else if let json = self.checkResponse(response: response) {
                    let data = self.dataExtractor(json: json)
                    success?(json, data)
                } else {
                    if let statusCode = response.response?.statusCode, statusCode == 401 {
                        self.authControl.setLoggedOut()
                    }
                    Log.error("""
                    URL: \(response.request?.url?.absoluteString ?? "nil")
                    Request: \(response.request?.description ?? "nil")
                    Response: \(response.response?.description ?? "nil")
                    Error: \(response.error?.localizedDescription ?? "nil")
                    """)

                    error?(response)
                    isError = true
                }
                
                let delta = Date().timeIntervalSince1970 - from
                statistics.log(
                    isError ? .serverRequestFailed : .serverRequestPassed,
                    params: [.name: self.getUrl(),
                             .time: "\(delta)"])
            }
    }
}


class AuthenticatedMethod: BaseNetworkMethod {
    override func getHeaders() -> [String : String] {
        var headers = super.getHeaders()

        if let token = authChecker.getAuthToken() {
            AuthenticatedMethod.getAuthHeaders(token: token)
                .forEach { headers[$0.key] = $0.value
            }
        }
        
        return headers
    }
    
    class func getAuthHeaders(token: String) -> [String: String] {
        var headers = [String: String]()
        headers["Authorization"] = "Bearer \(token)"
        return headers
    }
}
