//
//  NSCoding+Codable.swift
//  togezzer-ios
//
//  Created by Семен Власов on 11.05.2018.
//  Copyright © 2018 tgz. All rights reserved.
//

import Foundation

enum DataCodingKeys: CodingKey {
    case data
}

extension Encodable where Self: NSCoding {
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: DataCodingKeys.self)
        let data = NSKeyedArchiver.archivedData(withRootObject: self)
        try container.encode(data, forKey: .data)
    }
}

extension Decodable where Self: NSCoding {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: DataCodingKeys.self)
        let data = (try values.decode(Data.self, forKey: .data))
        if let obj = NSKeyedUnarchiver.unarchiveObject(with: data) as? Self {
            self = obj
        } else {
            let error = DecodingError.typeMismatch(
                Self.self,
                DecodingError.Context(codingPath: [DataCodingKeys.data], debugDescription: "")
            )
            throw error
        }
    }
}
