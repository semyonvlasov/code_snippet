//
//  Logger.swift
//  togezzer-ios
//
//  Created by Семен Власов on 22.05.2018.
//  Copyright © 2018 tgz. All rights reserved.
//

import Foundation
import CocoaLumberjack
import Crashlytics

var Log: TZLog { return TZLog.shared }

private final class CrashlyticsDestination: DDAbstractLogger {
    override func log(message logMessage: DDLogMessage) {
        let fileName = (logMessage.file as NSString).lastPathComponent

        let message = """
        File: \(fileName)
        Function: \(logMessage.function?.description ?? "nil")
        Line: \(logMessage.line)
        Queue: \(logMessage.queueLabel)
        Message: \(logMessage.message)
        """
        Crashlytics.sharedInstance().recordCustomExceptionName(
            "Custom Exception",
            reason: message,
            frameArray: []
        )
    }
}

final class TZLog {

    static let shared = TZLog()

    //MARK: - Constants

    #if MASTER
    let logLevel = DDLogLevel.error
    #elseif LOCAL
    let logLevel = DDLogLevel.debug
    #else
    let logLevel = DDLogLevel.all
    #endif

    #if DEBUG
    private let useNSLog = false
    private let useFileLog = true
    #else
    private let useNSLog = true
    private let useFileLog = true
    #endif

    private var fileLogger: DDFileLogger?
    var logFileURL: URL? {
        guard let filePath = fileLogger?.currentLogFileInfo.filePath else {
            return nil
        }
        return URL(fileURLWithPath: filePath)
    }

    // MARK: - Init

    init() {
        setupLogger()
    }

    //MARK: - Properties

    private func setupLogger() {
        asyncLoggingEnabled = false
        dynamicLogLevel = logLevel

        if #available(iOS 10.0, *), useNSLog {
            DDLog.add(DDOSLogger.sharedInstance, with: .verbose)
        } else {
            DDLog.add(DDTTYLogger.sharedInstance, with: .verbose)
            DDLog.add(DDASLLogger.sharedInstance, with: .verbose)
        }

        let crashlytics = CrashlyticsDestination()
        DDLog.add(crashlytics, with: .warning)

        if useFileLog {
            let fileLogger = DDFileLogger()
            fileLogger.rollingFrequency = 60 * 60 * 24
            fileLogger.logFileManager.maximumNumberOfLogFiles = 1
            DDLog.add(fileLogger, with: .verbose)
        }
    }

    // MARK: - Public

    func info(_ message: String) {
        DDLogInfo("Info: \(message)")
    }

    func verbose(
        _ message: String,
        file: StaticString = #file,
        function: StaticString = #function,
        line: UInt = #line
    ) {
        DDLogVerbose("Verbose: \(message)", file: file, function: function, line: line)
    }

    func warning(
        _ message: String,
        file: StaticString = #file,
        function: StaticString = #function,
        line: UInt = #line
    ) {
        DDLogWarn("Warning⚠️: \(message)", file: file, function: function, line: line)
    }

    func error(
        _ message: String,
        file: StaticString = #file,
        function: StaticString = #function,
        line: UInt = #line
    ) {
        DDLogError("Error❌: \(message)", file: file, function: function, line: line)
    }

    func debug(
        _ message: String,
        file: StaticString = #file,
        function: StaticString = #function,
        line: UInt = #line
    ) {
        DDLogDebug("Debug: \(message)", file: file, function: function, line: line)
    }

    func error(
        _ error: Error,
        file: StaticString = #file,
        function: StaticString = #function,
        line: UInt = #line
    ) {
        let message = "Error❌: \(error.localizedDescription)"
        DDLogError(message, file: file, function: function, line: line)
    }
}
