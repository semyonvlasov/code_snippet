//
//  SessionTabContainer.swift
//  togezzer-ios
//
//  Created by Семен Власов on 13/03/2019.
//  Copyright © 2019 tgz. All rights reserved.
//

import Dip

enum MainMenuEntryLocation {
    case companyCreated
    case companyRequestAccepted
    case normalLogin
}

typealias MainMenuModule = (input: MainMenuModuleInput, view: UIViewController)

class MainMenuTabContainer: BaseContainer {
    
    override var isGlobalAccessible: Bool { return true }
    
    var screensContainer: MainMenuScreensContainer!
    
    init(nc: UINavigationController?) {
        super.init()
        
        screensContainer = MainMenuScreensContainer()
        
        container.register(.singleton) { MainMenuTabVC() }
            .implements(MainMenuTabViewInput.self)
            .implements(INavigationStackProvider.self)
            .resolvingProperties { [weak self] _, view in
                view.presenter = try! self!.resolve()
        }
        
        container.register(.singleton) { MainMenuInteractor() }
            .implements(MainMenuInteractorInput.self)
            .resolvingProperties { [weak self] _, interactor in
                interactor.unreadChecker = try! self!.resolve()
                interactor.storageService = try! self!.resolve()
                interactor.linkHandler = try! self!.resolve()
                interactor.presenter = try! self!.resolve()
                interactor.apnService = try! self!.resolve()
                interactor.companyManager = try! self!.resolve()
        }
        
        container.register(.singleton) { MainMenuTabPresenter() }
            .implements(MainMenuInteractorOutput.self)
            .implements(MainMenuTabViewOutput.self)
            .implements(MainMenuModuleInput.self)
            .resolvingProperties { [weak self] _, presenter in
                presenter.view = try! self!.resolve()
                presenter.interactor = try! self!.resolve()
                presenter.router = try! self!.resolve()
                presenter.dialogsModule = try! self!.resolve()
                presenter.cardsModule = try! self!.resolve()
                presenter.mySpaceModule = try! self!.resolve()
                presenter.menuNavigator = try! self!.resolve()
            }
        
        container.register(.singleton) { MainMenuRouter() }
            .implements(MainMenuRouterInput.self)
            .resolvingProperties { _, router in
            router.nc = nc
        }
        
        container.register(.singleton) { MainMenuNavigator() }
            .implements(IMainMenuNavigator.self)
            .implements(MainMenuNavigatorInput.self)
            .implements(ShowingChatModuleOutput.self)
            .implements(ShowingCardModuleOutput.self)
            .implements(ShowingUserModuleOutput.self)
            .implements(IShowCompanyInviteNavigation.self)
            .resolvingProperties { [weak self] _, navigator in
                navigator.navigationStackProvider = try! self!.resolve()
                navigator.dbStorage = try! self!.resolve()
                navigator.userSettings = try! self!.resolve()
                navigator.universalLinkProvider = try! self!.resolve()
                let apnService: APNService = try! self!.resolve()
                apnService.sessionNavigator = navigator
        }
        
        container.register(.shared) { [weak self] () -> MainMenuModule in
            let view: MainMenuTabVC = try! self!.resolve()
            let input: MainMenuModuleInput = try! self!.resolve()
            
            return (input, view)
        }
        
        container.collaborate(with: screensContainer.container)
    }
    
    func getModule() -> MainMenuModule {
        return try! self.resolve()
    }
}

