//
//  Coordinator.swift
//  togezzer-ios
//
//  Created by Семен Власов on 18.03.2018.
//  Copyright © 2018 tgz. All rights reserved.
//

protocol ICoordinator: class, Navigation {
    func start()
}

protocol IStackCoordinator: ICoordinator {
    func closeScreensStack(animated: Bool)
    func start(closePreviousScreens: Bool)
}

protocol IStackCoordinatorDefault: IStackCoordinator {
    var firstFlowVC: UIViewController? { get set }
    var lastFlowVC: UIViewController? { get set }
    
    func preCloseActions()
}

extension IStackCoordinatorDefault {
    func closeScreensStack(animated: Bool) {
        guard
            let firstVC = firstFlowVC,
            let lastVC = lastFlowVC,
            let firstIndex = nc?.viewControllers.firstIndex(of: firstVC),
            let lastIndex = nc?.viewControllers.firstIndex(of: lastVC),
            firstIndex <= lastIndex
        else { return }
    
        preCloseActions()
        
        if lastVC == nc?.viewControllers.last {
            if firstIndex > 0,
                let popVC = nc?.viewControllers[safe: firstIndex - 1] {
                nc?.popToViewController(popVC, animated: animated)
            } else {
                nc?.popToRootViewController(animated: animated)
            }
        } else {
            var newStack = nc?.viewControllers ?? []
            newStack.removeSubrange(firstIndex...lastIndex)
            nc?.viewControllers = newStack
        }
        
        firstFlowVC = nil
        lastFlowVC = nil
    }
    
    func preCloseActions() {}
    
    func start(closePreviousScreens: Bool) {
        nc?.setViewControllers([], animated: false)
        start()
    }
    
    func stackScreen<S: RoutableModule>(_ screen: S,
                                        flowItemType: FlowItemType = .middle,
                                        animated: Bool = true,
                                        handler: ((S.T) -> Void)? = nil) {
        screen.flowItemType = flowItemType
        screen.eventHandler = handler
        stackVC(screen.vc, animated: animated)
    }
    
    func stackVC(_ vc: UIViewController, animated: Bool = true) {
        push(vc: vc, animated: animated)
        
        if firstFlowVC == nil {
            firstFlowVC = vc
        }
        lastFlowVC = vc
    }
}

typealias RoutableModule = IFlowController & ViewProvider & FlowInfo
extension ICoordinator {
    @discardableResult func pushScreen<S: RoutableModule>(
        screenFabric: () -> S,
        flowItemType: FlowItemType = .middle,
        animated: Bool = true,
        handler: ((S.T) -> Void)? = nil
    ) -> S {
        let screen = screenFabric()
        screen.flowItemType = flowItemType
        screen.eventHandler = handler
        push(vc: screen.vc, animated: true)
        return screen
    }
}
