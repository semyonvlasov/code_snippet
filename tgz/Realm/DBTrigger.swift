//
//  DBTrigger.swift
//  togezzer-ios
//
//  Created by Семен Власов on 25/03/2019.
//  Copyright © 2019 tgz. All rights reserved.
//

import Foundation

struct DBTrigger<DBO> {
    let keyPath: PartialKeyPath<DBO>
    private let predicate: ((Any) -> Bool)?
    let equalPredicate: (Any?, Any?) -> Bool
    let stringRepresentaion: String?
    
    init<T: Equatable>(keyPath: KeyPath<DBO, T>, predicate: ((T) -> Bool)?) {
        self.keyPath = keyPath
        self.stringRepresentaion = NSExpression(forKeyPath: keyPath).keyPath
        
        if let predicate = predicate {
            self.predicate = { value in
                let typedValue = value as! T
                return predicate(typedValue)
            }
        } else {
            self.predicate = nil
        }
        
        self.equalPredicate = { old, new in
            if old == nil && new == nil { return true }
            guard let old = old as? T, let new = new as? T else { return false }
            
            return old == new
        }
    }
    
    func matchesAgainst(_ object: DBO) -> Bool {
        let value = object[keyPath: keyPath]
        return predicate?(value) ?? true
    }
    
    static func apply<DBO>(_ triggers: [DBTrigger<DBO>], to collection: [DBO]) -> [DBO] {
        return collection.filter { object in
            for trigger in triggers {
                guard trigger.matchesAgainst(object) else { return false }
            }
            return true
        }
    }
}

//MARK: - DB Object changes filter
extension DBTrigger {
    static func any<T: Equatable>(_ keyPath: KeyPath<DBO, T>) -> DBTrigger<DBO> {
        return DBTrigger(keyPath: keyPath, predicate: nil)
    }
}
