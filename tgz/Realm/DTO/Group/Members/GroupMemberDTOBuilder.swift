//
//  GroupMemberDTOBuilder.swift
//  togezzer-ios
//
//  Created by Семен Власов on 21/08/2019.
//  Copyright © 2019 tgz. All rights reserved.
//

import RealmSwift

class GroupMemberDTOBuilder: DTObjectBuilder<GroupMemberDTO, UserGroupInfo> {
    
    class func get(userId: Int?, groupId: Int?, realm: Realm? = nil) -> GroupMemberDTO? {
        guard
            let userId = userId,
            let realm = realm ?? (try? Realm()),
            let groupId = groupId,
            let dbObject = UserGroupInfo.get(groupId: groupId, userId: userId),
            let dto = GroupMemberDTOBuilder.translate(dbObject: dbObject, realm: realm)
        else { return nil }
        
        return dto
    }
    
    override class func translateNonEmpty(dbObject: UserGroupInfo,
                                          realm: Realm) -> GroupMemberDTO? {
        guard
            let baseDTO = getBaseDTO(dbObject: dbObject),
            let userModel = CompanyUserDTOBuilder
                .translate(dbObject: dbObject.userInfo, realm: realm),
            let status = UserGroupStatus(rawValue: dbObject.groupStatus)
        else {
            Log.error("GroupMemberDTOBuilder: can't create dto from: \(dbObject)")
            return nil
        }
        guard let groupRole = GroupRole(rawValue: dbObject.role) else {
            return nil
        }
        return GroupMemberDTO(
            baseDTO: baseDTO,
            userInfo: userModel,
            groupStatus: status,
            role: groupRole,
            groupId: dbObject.group.id
        )
    }
}
