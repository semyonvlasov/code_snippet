//
//  UserDefaultsKeys.swift
//  togezzer-ios
//
//  Created by Семен Власов on 05.04.2018.
//  Copyright © 2018 tgz. All rights reserved.
//

import Foundation

enum UDKey: String {
    case lastAPNState
    ...
}

extension DefaultsKeys {
    
    //MARK: - Global keys
    static let lastAPNState = DefaultsKey<APNAuthorizationStatus>(.lastAPNState)
}

extension DefaultsKey {
    convenience init(_ key: UDKey) {
        let value = key.rawValue
        self.init(value)
    }
}
