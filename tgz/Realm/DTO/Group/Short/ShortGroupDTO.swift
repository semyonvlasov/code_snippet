//
//  ShortGroupDTO.swift
//  togezzer-ios
//
//  Created by Семен Власов on 04/10/2019.
//  Copyright © 2019 tgz. All rights reserved.
//

struct ShortGroupDTO: ShortGroupDTOProtocol {
    let baseDTO: DTObjectProtocol
    
    let type: GroupType
    let title: String
    let avatar: AvatarModel
}

protocol ShortGroupDTOProtocol: DecoratedDTObjectProtocol {
    var type: GroupType { get }
    var title: String { get }
    var avatar: AvatarModel{ get }
}

protocol DecoratedShortGroupDTOProtocol: ShortGroupDTOProtocol {
    var shortGroupDTO: ShortGroupDTOProtocol { get }
}

extension DecoratedShortGroupDTOProtocol {
    var baseDTO: DTObjectProtocol { return shortGroupDTO.baseDTO }
    
    var type: GroupType { return shortGroupDTO.type }
    var title: String { return shortGroupDTO.title }
    var avatar: AvatarModel{ return shortGroupDTO.avatar }
}
