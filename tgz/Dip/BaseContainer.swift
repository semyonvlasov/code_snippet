//
//  BaseContainer.swift
//  togezzer-ios
//
//  Created by semyonvlasov on 18/01/2018.
//  Copyright © 2018 tgz. All rights reserved.
//

import Foundation
import Dip

enum GlobalResolveError: Error {
    case notFound
}

class BaseContainer {
    let container = DependencyContainer()
    var isGlobalAccessible: Bool { return false }
    
    init() {
        if isGlobalAccessible {
            ContainersProvider.shared.addContainer(container)
        }
    }
    
    func resolve<T>(tag: DependencyTagConvertible? = nil) throws -> T {
        let resolved = threadSafe { () -> T? in
            if let ret = try? container.resolve(tag: tag) as T {
                return ret
            }
            for globalContainer in ContainersProvider.shared.getContainers() {
                if let ret = try? globalContainer.resolve(tag: tag) as T {
                    return ret
                }
            }
            return nil
        }
        if let resolved = resolved {
            return resolved
        }
        throw GlobalResolveError.notFound
    }
    
    private func threadSafe<T>(_ work: () -> T) -> T {
        if Thread.isMainThread {
            return work()
        } else {
            return DispatchQueue.main.sync {
                return work()
            }
        }
    }
}
