//
//  BWDImageLoader.h
//  camDrive
//
//  Created by adm on 14.01.16.
//  Copyright © 2016 adm. All rights reserved.
//


@protocol BWDImageLoadDelegate <NSObject>

- (void)imageDidLoadFromUrl:(NSString *)urlStr;

@optional
- (void)imageLoadFailedFromUrl:(NSString *)urlStr;

@end

@interface BWDImageLoader : NSObject

+ (void)loadImgWithUrl:(NSString *)imgUrl completion:(void(^)(UIImage *img))completion failure:(void(^)(NSError *error))failure;

@end
