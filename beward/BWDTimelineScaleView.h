//
//  BWDTimelineScaleView.h
//  testTimeline
//
//  Created by adm on 23.12.15.
//  Copyright © 2015 adm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BWDTimelineRedrawnView.h"

@interface BWDTimelineScaleView : BWDTimelineRedrawnView

@property(nonatomic, strong) UIColor *scaleColor;
@property(nonatomic, strong) UIFont *scaleFont;
@property(nonatomic, assign) Boolean digitsUnderScale;

@end
