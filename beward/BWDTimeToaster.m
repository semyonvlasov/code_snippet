//
//  BWDTimeToaster.m
//  camDrive
//
//  Created by adm on 19.01.16.
//  Copyright © 2016 adm. All rights reserved.
//

#import "BWDTimeToaster.h"
#import "NSDate+Utilities.h"

@implementation BWDTimeToaster {
    NSTimer *_hideTimer;
    BOOL _isVisible;
}

static float const kAppearDuration = 1.0;
static float const kFadeInAnimationDuration = 0.1;
static float const kFadeOutAnimationDuration = 0.3;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.alpha = 0;
        self.backgroundColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:.6];
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = YES;
        self.font = [UIFont systemFontOfSize:14];
        self.textColor = [UIColor whiteColor];
        self.adjustsFontSizeToFitWidth = NO;
        self.textAlignment = NSTextAlignmentCenter;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.alpha = 0;
        self.backgroundColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:.6];
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = YES;
        self.font = [UIFont systemFontOfSize:14];
        self.textColor = [UIColor whiteColor];
        self.textAlignment = NSTextAlignmentCenter;
    }
    return self;
}

- (void)drawTextInRect:(CGRect)uiLabelRect {
    [super drawTextInRect:UIEdgeInsetsInsetRect(uiLabelRect, UIEdgeInsetsMake(0,5,0,5))];
}

- (CGSize) intrinsicContentSize {
    CGSize intrinsicSuperViewContentSize = [super intrinsicContentSize];
    intrinsicSuperViewContentSize.width += 10;
    return intrinsicSuperViewContentSize ;
}

- (void)showToasterWithTime:(NSDate *)time {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss"];
    [self showToasterWithString:[formatter stringFromDate:time]];
}

- (void)showToasterWithDate:(NSDate *)date {
    //NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"yyyy-MM-dd"];
    [self showToasterWithString:[date mediumDateString]];
}

- (void)showToasterWithString:(NSString *)str {
    self.text = str;
    [self fadeInToaster];
}

- (void)fadeInToaster {
    //[self.layer removeAllAnimations];
    [_hideTimer invalidate];
    _hideTimer = nil;
    float timerDelay = kAppearDuration;
    if (!_isVisible) {
        _isVisible = YES;
        timerDelay += kFadeInAnimationDuration;
        [UIView animateWithDuration:kFadeInAnimationDuration animations:^{
            self.alpha = 1;
        }];
    }
    _hideTimer = [NSTimer scheduledTimerWithTimeInterval:timerDelay target:self selector:@selector(fadeOutToaster) userInfo:nil repeats:NO];
    

}

- (void)fadeOutToaster {
    [UIView animateWithDuration:kFadeOutAnimationDuration animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        _isVisible = NO;
    }];
}

@end
