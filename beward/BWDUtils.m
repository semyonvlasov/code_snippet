//
//  BWDUtils.m
//  camDrive
//
//  Created by adm on 27.11.15.
//  Copyright © 2015 adm All rights reserved.
//

#import "BWDUtils.h"
#import "BWDStringDefines.h"
#import <sys/sysctl.h>

@implementation BWDUtils

+ (NSString *)getUserAgentString {
    NSString *iosVer = [UIDevice currentDevice].systemVersion;
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *deviceType = [self platformString];
    return [NSString stringWithFormat:@"Native CamDrive/%@ iOS %@; %@" , appVersion, iosVer, deviceType];

}

+ (NSString *) platformString {
    // Gets a string with the device model
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
    free(machine);
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,2"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"iPhone 4 (Verizon)";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (UK+Europe+Asia+China)";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (UK+Europe+Asia+China)";
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad mini-1G (WiFi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad mini-1G (GSM)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad mini-1G (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad-3G (WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad-3G (4G)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad-3G (4G)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad-4G (WiFi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad-4G (GSM)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad-4G (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air (WiFi)";
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad4,3"])      return @"iPad Air (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPad Mini Retina (WiFi)";
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPad Mini Retina (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad5,3"])      return @"iPad Air 2 (WiFi)";
    if ([platform isEqualToString:@"iPad5,4"])      return @"iPad Air 2";
    
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    return platform; }

/*+ (NSUInteger)getLineNumberForLabel:(UILabel *)label {
    CGSize sizeConstraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    NSString *text = label.text? label.text: @"W";
    CGSize singleLineSize;
    CGSize controlSize;
    NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:label.font forKey: NSFontAttributeName];
    singleLineSize = [@"W" boundingRectWithSize:sizeConstraint
                                     options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin
                                  attributes:stringAttributes
                                     context:nil].size;
    
    controlSize = [text boundingRectWithSize:sizeConstraint
                                        options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin
                                     attributes:stringAttributes
                                        context:nil].size;
    
    return (int)ceilf(controlSize.height / singleLineSize.height);
}*/

+ (CGFloat)getLineHeightForFont:(UIFont *)font {
    NSString *w = @"W";
    CGSize remainingSize = (CGSize){100, 100};
    NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:font forKey: NSFontAttributeName];
    CGSize size = [w boundingRectWithSize:remainingSize
                           options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin
                        attributes:stringAttributes
                           context:nil].size;
    return size.height;
}

+ (void)borderColorChangeAnimation:(NSArray*)viewArray toColor:(UIColor *)color {
    if (viewArray != nil && [viewArray count] > 0) {
        UIColor *newColor = color;
        if (newColor == nil) newColor = UIColorFromRGB(0xD80E17);
        
        [CATransaction begin];
        for (UIView *v in viewArray) {
            if (v.layer.borderColor != nil) {
                CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"borderColor"];
                anim.fromValue = (id)v.layer.borderColor;
                anim.toValue = (id)[newColor CGColor];
                anim.duration = 0.3;
                anim.removedOnCompletion = NO;
                anim.fillMode = kCAFillModeBoth;
                anim.timingFunction = [CATransaction animationTimingFunction];
                [v.layer addAnimation:anim forKey:@"myAnimation"];
            }
        }
        [CATransaction setCompletionBlock:^{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                for (UIView *v in viewArray) {
                    if (v.layer.borderColor != nil && (int)v.alpha > 0) {
                        v.alpha = v.alpha + 0.05;
                        
                        CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"borderColor"];
                        anim.fromValue = (id)[newColor CGColor];
                        anim.toValue = (id)v.layer.borderColor;
                        anim.duration = 0.4;
                        anim.timingFunction = [CATransaction animationTimingFunction];
                        [v.layer addAnimation:anim forKey:@"myAnimation"];
                    }
                }
            });
        }];
        [CATransaction commit];
    }
}

+ (void)shakeAnimation:(NSArray*) viewArray {
    const int reset = 5;
    const int maxShakes = 6;
    
    static int shakes = 0;
    static int translate = reset;
    
    [UIView animateWithDuration:0.09-(shakes*.01)
                          delay:0.01f
                        options:(enum UIViewAnimationOptions) UIViewAnimationCurveEaseInOut
                     animations:^{
                         for (int i = 0; i < viewArray.count; ++i)
                         {
                             UIView *view = [viewArray objectAtIndex:i];
                             view.transform = CGAffineTransformMakeTranslation(translate, 0);
                         }
                     }
                     completion:^(BOOL finished){
                         if(shakes < maxShakes){
                             shakes++;
                             
                             if (translate>0)
                                 translate--;
                             
                             translate*=-1;
                             [self shakeAnimation:viewArray];
                         } else {
                             for (int i = 0; i < viewArray.count; ++i)
                             {
                                 UIView *view = [viewArray objectAtIndex:i];
                                 view.transform = CGAffineTransformIdentity;
                             }
                             shakes = 0;
                             translate = reset;
                             return;
                         }
                     }];
}

+ (NSDate *)dateOnlyFromDate:(NSDate *)date {
    unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:flags fromDate:date];
    return [calendar dateFromComponents:components];
}

+ (NSDate *)yesterdayFromDate:(NSDate *)date {
    return [BWDUtils dateOnlyFromDate:[NSDate dateWithTimeInterval:-86400 sinceDate:date]];
}

+ (NSDate *)tomorrowFromDate:(NSDate *)date {
    return [BWDUtils dateOnlyFromDate:[NSDate dateWithTimeInterval:86400 sinceDate:date]];
}

+ (NSDate *)weekStartFromDate:(NSDate *)date {
    NSCalendar *gregorian = [NSCalendar currentCalendar];
    
    // Get the weekday component of the current date
    NSDateComponents *weekdayComponents = [gregorian components:NSWeekdayCalendarUnit fromDate:date];

    NSDateComponents *componentsToSubtract = [[NSDateComponents alloc] init];
    /* Substract [gregorian firstWeekday] to handle first day of the week being something else than Sunday */
    [componentsToSubtract setDay: - ([weekdayComponents weekday] - [gregorian firstWeekday])];
    NSDate *beginningOfWeek = [gregorian dateByAddingComponents:componentsToSubtract toDate:date options:0];
    
    NSDateComponents *components = [gregorian components: (NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit)
                                                fromDate: beginningOfWeek];
    beginningOfWeek = [gregorian dateFromComponents: components];
    return beginningOfWeek;
}

+ (BOOL)isSaturdayDate:(NSDate *)date {
    NSCalendar *gregorian = [NSCalendar currentCalendar];
    NSDateComponents *weekdayComponents = [gregorian components:NSWeekdayCalendarUnit fromDate:date];
    return [weekdayComponents weekday] == 7;
}

+ (BOOL)isSundayDate:(NSDate *)date {
    NSCalendar *gregorian = [NSCalendar currentCalendar];
    NSDateComponents *weekdayComponents = [gregorian components:NSWeekdayCalendarUnit fromDate:date];
    return [weekdayComponents weekday] == 1;
}

+ (NSString *)genRandomIdWithLength:(unsigned)length {
    if (length < 500) {
        NSString *alphabet  = @"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        u_int32_t aL = (u_int32_t)alphabet.length;
        NSMutableString *s = [NSMutableString stringWithCapacity:16];
        for (int i = 0; i < length; i++) {
            u_int32_t r = arc4random_uniform(aL);
            unichar c = [alphabet characterAtIndex:r];
            [s appendFormat:@"%C", c];
        }
        return s;
    }
    return nil;
}

@end
