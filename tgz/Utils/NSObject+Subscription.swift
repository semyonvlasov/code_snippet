//
//  NSObject+Subscription.swift
//  togezzer-ios
//
//  Created by Семен Власов on 10.04.2018.
//  Copyright © 2018 tgz. All rights reserved.
//

import Foundation

typealias NotificationCenterHandler = (Notification) -> Void

protocol Subscription {
    func subscribe(event: Notification.Name,
                   handler: @escaping(NotificationCenterHandler))
    func subscribe(event: Notification.Name, action: Selector)
    func removeSubscription(event: Notification.Name)
    func removeSubscriptions()
}

extension NSObject: Subscription {
    private struct AssociatedKeys {
        static var descriptiveName = "tzz_NotificationObjects"
    }
    
    var tzzSubscriptionObjects: NSDictionary? {
        get {
            return (
                objc_getAssociatedObject(
                    self,
                    &AssociatedKeys.descriptiveName
                ) as? NSDictionary
            )
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(
                    self,
                    &AssociatedKeys.descriptiveName,
                    newValue as NSDictionary?,
                    .OBJC_ASSOCIATION_RETAIN_NONATOMIC
                )
            }
        }
    }
        
    func subscribe(event: Notification.Name,
                   handler: @escaping(NotificationCenterHandler)) {
        let notificationObject = NotificationCenter.default
            .addObserver(forName: event, object: nil, queue: .main, using: handler)
        
        let dict = tzzSubscriptionObjects ?? NSDictionary()
        let objects = NSMutableDictionary(dictionary: dict)
        if let oldObj = objects[event.rawValue  as NSString] {
            NotificationCenter.default.removeObserver(oldObj)
        }
        
        objects.setObject(notificationObject, forKey: event.rawValue as NSString)
        tzzSubscriptionObjects = objects
    }
    
    func subscribe(event: Notification.Name, action: Selector) {
        NotificationCenter.default.addObserver(self,
                                               selector: action,
                                               name: event,
                                               object: nil)
    }
    
    func removeSubscription(event: Notification.Name) {
        NotificationCenter.default.removeObserver(self, name: event, object: nil)
    }
    
    func removeSubscriptions() {
        tzzSubscriptionObjects?.allValues.forEach {
            NotificationCenter.default.removeObserver($0)
        }
        NotificationCenter.default.removeObserver(self)
    }
}
