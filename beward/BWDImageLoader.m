//
//  BWDImageLoader.m
//  camDrive
//
//  Created by adm on 14.01.16.
//  Copyright © 2016 adm. All rights reserved.
//

#import "BWDImageLoader.h"
#import "AFNetworking.h"
#import "NSDictionary+NSNull.h"
#import "NSString+Escaping.h"

@implementation BWDImageLoader

+ (void)loadImgWithUrl:(NSString *)imgUrl completion:(void(^)(UIImage *img))completion failure:(void(^)(NSError *error))failure {
    NSURL *url = [NSURL URLWithString:imgUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"image/jpeg", @"application/json", nil];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        UIImage *img = [UIImage imageWithData:responseObject];

        if (completion != nil) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                completion(img);
            }];
            }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        if (failure != nil) {
            failure(error);
        }
    }];
    
    [operation start];
}

@end
