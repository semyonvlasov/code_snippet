//
//  DBSaver.swift
//  togezzer-ios
//
//  Created by Семен Власов on 06/08/2019.
//  Copyright © 2019 tgz. All rights reserved.
//

import Foundation
import RealmSwift

struct DBSaver<Saver: DBModelSaver> {
    
    typealias Model = Saver.Model
    typealias DBO = Saver.DBO
    
    @discardableResult
    static func save(models: [Model], realm: Realm? = nil) -> [DBO] {
        guard let realm = realm ?? (try? Realm()) else {
            assertionFailure("Cant init realm")
            return []
        }
        
        var ret: [DBO] = []
        
        do {
            try realm.safeWrite {
                ret = transaction(models: models, realm: realm)
            }
        } catch {
            assertionFailure("Failed save models")
        }
        
        return ret
    }
    
    @discardableResult
    static func save(model: Model?, realm: Realm? = nil) -> DBO? {
        guard let model = model else { return nil }
        return save(models: [model], realm: realm).first
    }
    
    ///Для обновления созданных на девайсе моделек (например, новых сообщений)
    @discardableResult
    static func forceMerge(model: Model, toId internalId: Int) -> Int? {
        guard
            let realm = try? Realm(),
            let remoteId = model.remoteId,
            let dbObject = DBO.get(id: internalId, realm: realm)
        else { return nil }
        
        try? realm.safeWrite {
            let copy = DBO(value: dbObject, schema: .partialPrivateShared())
            copy.id = remoteId
            copy.isSync = true
            copy.isSyncError = false
            copy.isEmpty = false
            realm.delete(dbObject)
            Saver.merge(model: model, dbObject: copy, realm: realm)
            realm.add(copy, update: true)
        }
        
        return remoteId
    }
    
    private static func transaction(models: [Model], realm: Realm) -> [DBO] {
        var ret: [DBO] = []
        
        for model in models {
            let dbObject = DBO.get(id: model.remoteId) ?? DBO()
            
            if dbObject.id == 0 {
                dbObject.id = model.remoteId ?? 0
                realm.add(dbObject, update: true)
            }
            
            Saver.merge(model: model, dbObject: dbObject, realm: realm)
            
            dbObject.isSync = true
            dbObject.isSyncError = false
            dbObject.isEmpty = false
            
            Saver.linkExternal(model: model, dbObject: dbObject, realm: realm)
            ret.append(dbObject)
        }
        
        return ret
    }
}
