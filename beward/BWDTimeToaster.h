//
//  BWDTimeToaster.h
//  camDrive
//
//  Created by adm on 19.01.16.
//  Copyright © 2016 adm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BWDTimeToaster : UILabel

- (id)initWithCoder:(NSCoder *)aDecoder;

- (void)showToasterWithTime:(NSDate *)time;
- (void)showToasterWithDate:(NSDate *)date;
- (void)showToasterWithString:(NSString *)str;

@end
