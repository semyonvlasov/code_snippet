//
//  GroupDTO.swift
//  togezzer-ios
//
//  Created by kost ant on 06/12/2018.
//  Copyright © 2018 tgz. All rights reserved.
//

struct GroupDTO: GroupDTOProtocol {
    let shortGroupDTO: ShortGroupDTOProtocol

    let groupDescription: String
    let chatId: Int
    let companyId: Int
    let membersCount: Int
    let userIds: [Int]
    let author: DBLink<UserDTO>
    let userRole: GroupRole?
}

protocol GroupDTOProtocol: DecoratedShortGroupDTOProtocol {
    var groupDescription: String { get }
    var chatId: Int { get }
    var companyId: Int { get }
    var membersCount: Int { get }
    var userIds: [Int] { get }
    var author: DBLink<UserDTO> { get }
    var userRole: GroupRole? { get }
}
