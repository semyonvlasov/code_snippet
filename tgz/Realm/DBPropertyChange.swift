//
//  DBPropertyChange.swift
//  togezzer-ios
//
//  Created by Семен Власов on 26/03/2019.
//  Copyright © 2019 tgz. All rights reserved.
//

import Foundation

struct DBPropertyChange<DBO> {
    
    typealias Value<T> = (old: T, new: T)
    
    let key: PartialKeyPath<DBO>
    let oldValue: Any?
    let newValue: Any?
    
    func extract<T>(key: KeyPath<DBO, T>) -> Value<T>? {
        guard
            key == self.key,
            let oldValue = oldValue as? T,
            let newValue = newValue as? T
        else { return nil }
        
        return (old: oldValue, new: newValue)
    }
}
