//
//  SingleMethodTask.swift
//  togezzer-ios
//
//  Created by semyonvlasov on 16.10.2017.
//  Copyright © 2017 tgz. All rights reserved.
//

import Foundation
import JSONCodable
import PromiseKit

enum TaskError: Error {
    case dummy
}

class SingleMethodTask<SuccessData>: TaskProtocol  where SuccessData: Any {
    
    var method: BaseNetworkMethod?
    
    var successCallback: ((JSONObject, SuccessData?) -> Void)?
    var errorCallback: ((Any?) -> Void)?
    
    func execute() {
        TasksStorage.insert(task: self)
        
        preExecute()
        method?.perform(success: { [weak self] response, data in
            DispatchQueue.global().async {
                let processedData = self?.processData(data: data)

                DispatchQueue.main.async {
                    self?.onSuccess(json: response, data: processedData)
                    if let strongSelf = self {
                        TasksStorage.remove(task: strongSelf)
                    }
                }
            }
        }, error: { [weak self] error in
            guard let strongSelf = self else { return }
            
            strongSelf.onError(error)
            TasksStorage.remove(task: strongSelf)
        })
    }
    
    func processData(data: Any?) -> SuccessData? {
        return data as? SuccessData
    }
    
    func onSuccess(json: JSONObject, data: SuccessData?) {
        successCallback?(json, data)
    }
    
    func onError(_ error: Any?) {
        errorCallback?(error)
    }
    
    func preExecute() {}
    
    @discardableResult func success(callback: ((JSONObject, SuccessData?) -> Void)?) -> SingleMethodTask {
        self.successCallback = callback
        return self
    }
    
    @discardableResult func error(callback: ((Any?) -> Void)?)  -> SingleMethodTask {
        self.errorCallback = callback
        return self
    }
}

//MARK: Promise Kit Wrapper
extension SingleMethodTask {
    func fetch(completion: ((SuccessData?, Error?) -> Void)?) {
        success { completion?($1, nil) }
        error { completion?(nil, ($0 as? Error) ?? TaskError.dummy) }
        execute()
    }
    
    func fetch() -> Promise<SuccessData> {
        return Promise { fetch(completion: $0.resolve) }
    }
}
