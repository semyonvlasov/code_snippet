//
//  GroupDTOBuilder.swift
//  togezzer-ios
//
//  Created by kost ant on 06/12/2018.
//  Copyright © 2018 tgz. All rights reserved.
//

import RealmSwift

class GroupDTOBuilder: DTObjectBuilder<GroupDTO, Group> {
    
    override class func translateNonEmpty(dbObject: Group, realm: Realm) -> GroupDTO? {
        guard
            let shortDTO = ShortGroupDTOBuilder.translate(dbObject: dbObject, realm: realm)
        else {
            Log.error("GroupDTOBuilder Can't create dto from: \(dbObject)")
            return nil
        }
        
        let userIds: [Int] = dbObject.members
            .filter("groupStatus == %@", UserGroupStatus.active.rawValue)
            .map { $0.userInfo.user.id }

        let userRole: GroupRole?
        if let roleString = dbObject.myStatus?.role {
            userRole = GroupRole(rawValue: roleString)
        } else {
            userRole = nil
        }

        let description = shortDTO.type == .company && dbObject.groupDescription.isEmpty
            ? "general_chat_info_description".localized
            : dbObject.groupDescription

        return GroupDTO(
            shortGroupDTO: shortDTO,
            groupDescription: description,
            chatId: dbObject.chat.id,
            companyId: dbObject.company.id,
            membersCount: userIds.count,
            userIds: userIds,
            author: UserDTOBuilder.getLink(dbObject: dbObject.author.user, realm: realm),
            userRole: userRole
        )
    }
}
