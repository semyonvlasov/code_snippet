//
//  CollectionModuleCollectionModulePresenter.swift
//  togezzer-ios
//
//  Created by semyonvlasov on 18/01/2018.
//  Copyright © 2018 tgz. All rights reserved.
//

import UIKit
import SwipeCellKit

enum CollectionState {
    case ready
    case loading
    case empty
    case feed
}

class CollectionModulePresenter<DataSource>: NSObject, CollectionModuleViewOutput, CollectionDataSourceDelegateProtocol, SwipeTableViewCellDelegate, SwipeCollectionViewCellDelegate where
DataSource: CollectionDataSourceProtocol {
    
    typealias CellPresenter = CollectionItemPresenterProtocol
    typealias CompanionCollection = OrderedDictionary<CollectionItemCompanion>
    typealias CellType = CollectionTypeViewCellProtocol
    typealias CollectionType = CollectionTypeViewProtocol
    typealias ItemType = CollectionModelProtocol
    
    //MARK: - Properties
    let queue = SerialQueue()
    var state: CollectionState = .ready
    
    //Need to inject or set on viewDidLoad
    var dataSource: DataSource!
    
    var presenterFactory: CollectionItemPresenterFactory!
    var itemCompanionCollection: CompanionCollection = OrderedDictionary(items: [])
    var sections: [SectionModelProtocol] = [] { didSet {
        var offsets = [0]
        sections.forEach { offsets.append((offsets.last ?? 0) + $0.items ) }
        sectionOffsets = offsets
    } }
    private var sectionOffsets = [Int]()
    
    let presentersByCell = NSMapTable<CollectionTypeViewCellProtocol, AnyObject>.weakToWeakObjects()
    var visibleCellsList: [IndexPath: UICollectionViewCell] = [:] // @see visibleCellsAreValid(changes:)
    
    //MARK: - View Out
    func viewIsReady() {
        dataSource.delegate = self
        
        self.presenterFactory = self.createPresenterFactory()
        self.presenterFactory.configure(withCollectionView: getModuleCollectionTypeView())
    }
    
    func viewWillAppear() {
        if state != .loading {
            reloadOrRefreshFeeds()
        }
    }
    
    func viewDidAppear() {}
    
    func viewWillDissapear() {}
    
    func viewDidLayoutSubviews() {
        if getModuleView().isFirstLayout {
            self.queue.start()
        }
    }
    
    func reloadOrRefreshFeeds() {
        //Если экран с фидами не содержит элементов, то мы попытаемся их подрузить
        //Если они уже есть, то мы обновим выидимые
        if itemCompanionCollection.count == 0 {
            reloadAllCells()
        } else {
            reloadVisibleCells()
        }
    }
    
    func cellForRowAtIndexPath(indexPath: IndexPath,
                               collection: CollectionTypeViewProtocol) -> CollectionTypeViewCellProtocol {
        let presenter = self.presenterForIndexPath(indexPath)
        let cell = presenter.dequeueCell(collection, indexPath: indexPath)
        presenter.configureCell(cell, animated: false) { }
        
        if let swipeCell = cell as? SwipeTableViewCell {
            swipeCell.delegate = self
        }
        
        if let swipeCell = cell as? SwipeCollectionViewCell {
            swipeCell.delegate = self
        }
        
        return cell
    }
    
    func didEndDisplayingCell(cell: CollectionTypeViewCellProtocol,
                              for indexPath: IndexPath) {
        // Carefull: this index path can refer to old data source after an update. Don't use it to grab items from the model
        // Instead let's use a mapping presenter <--> cell
        if let oldPresenterForCell = self.presentersByCell.object(forKey: cell) as? CollectionItemPresenterProtocol {
            self.presentersByCell.removeObject(forKey: cell)
            oldPresenterForCell.cellWasHidden(cell)
        }
    }
    
    func willDisplayCell(cell: CollectionTypeViewCellProtocol,
                         for indexPath: IndexPath) {
        // Here indexPath should always referer to updated data source.
        let presenter = self.presenterForIndexPath(indexPath)
        self.presentersByCell.setObject(presenter, forKey: cell)
        presenter.cellWillBeShown(cell)
    }
    
    func heightForCell(_ indexPath: IndexPath) -> CGFloat? {
        let presenter = self.presenterForIndexPath(indexPath)
        return presenter.heightForCell(maximumWidth: nil)
        
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        return sections[section].items
    }
    
    func numberOfSections() -> Int {
        return sections.count
    }
    
    func cellDidPress(_ indexPath: IndexPath) {
        let presenter = presenterForIndexPath(indexPath)
        presenter.cellDidPress()
        if let visible = visibleCells(),
            let cell = visible[indexPath] {
            presenter.cellWillBeShown(cell)
        }
        
    }
    
    func shouldShowMenuForItemAtIndexPath(_ indexPath: IndexPath) -> Bool {
        return presenterForIndexPath(indexPath).shouldShowMenu()
    }
    
    func canPerformAction(_ action: Selector,
                          forItemAtIndexPath indexPath: IndexPath) -> Bool {
        return presenterForIndexPath(indexPath).canPerformMenuControllerAction(action)
    }
    
    func performAction(_ action: Selector,
                       forItemAtIndexPath indexPath: IndexPath) {
        presenterForIndexPath(indexPath).performMenuControllerAction(action)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        return presenterForIndexPath(indexPath).editActions(for: orientation)
    }
    
    func collectionView(_ collectionView: UICollectionView, editActionsForItemAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        return presenterForIndexPath(indexPath).editActions(for: orientation)
    }
    
    func tableView(_ tableView: UITableView,
                   editActionsOptionsForRowAt indexPath: IndexPath,
                   for orientation: SwipeActionsOrientation) -> SwipeOptions {
        return presenterForIndexPath(indexPath).editActionsOptions(for: orientation)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        editActionsOptionsForItemAt indexPath: IndexPath,
                        for orientation: SwipeActionsOrientation) -> SwipeOptions {
        return presenterForIndexPath(indexPath).editActionsOptions(for: orientation)
    }
    
    func getModuleView() -> CollectionModuleViewInput! {
        assert(false, "Override in subclass")
        return nil
    }
    
    final func getModuleCollectionTypeView() -> CollectionType {
        return getModuleView().collectionTypeView
    }
    
    final func createPresenterFactory() -> CollectionItemPresenterFactory {
        let builders = createPresenterBuilders()
        return CollectionItemPresenterFactory(presenterBuildersByType: builders)
    }
    
    func processDataSourceUpdate(isEmpty empty: Bool) { }
    
    //Все подклассы обязаны переопределить эту функцию
    //В ней требуется проставить соотношение между типами ячеек и билдерами презентеров
    func createPresenterBuilders() -> [CollectionItemType: CollectionItemPresenterBuilderProtocol] {
        assert(false, "Override in subclass")
        return [CollectionItemType: CollectionItemPresenterBuilderProtocol]()
    }
    
    func reloadVisibleCells() {
        guard
            let indexPathsForVisibleRows = getModuleCollectionTypeView()
                .indexPathsForVisibleCells()
        else { return }
        
        if state != .loading {
            state = .loading
            
            if dataSource.needReloadVisibleCells {
                dataSource.reload(indexPaths: indexPathsForVisibleRows) { [weak self] in
                    self?.dataSource.loadNew()
                }
            } else {
                dataSource.loadNew()
            }
        }
    }
    
    //Метод для обработки удалений SwipeCellKit
    func removeItem(uid: String) {
        if let indexPath = getIndexPath(uid: uid) {
            itemCompanionCollection.remove(uid: uid)
            sections[indexPath.section].items -= 1
        }
    }
    
    func reloadAllCells() {
        state = .loading
        dataSource.reload()
    }
    
    func loadPreviousCells() {
        if dataSource.hasMorePrevious {
            state = .loading
            dataSource.loadPrevious()
        }
    }
    
    func loadNewCells() {
        state = .loading
        dataSource.loadNew()
    }
    
    @discardableResult func createModelUpdates(
        newItemsData: ([ItemType], [SectionModelProtocol]),
        oldItemsData: (CompanionCollection, [SectionModelProtocol])
    ) -> (changes: CollectionChanges, updateModelClosure: () -> Void) {
        
        let itemCompanionCollection = createCompanionCollection(items: newItemsData.0)
        let changes = CollectionChangesDetector.generateChanges(
            oldCollectionData: (items:oldItemsData.0.lazy.map { $0.item },
                                sections: oldItemsData.1),
            newCollectionData: (items: itemCompanionCollection.lazy.map { $0.item },
                                sections:  newItemsData.1)
        )
        
        let updateModelClosure: () -> Void = { [weak self] in
            self?.itemCompanionCollection = itemCompanionCollection
            self?.sections = newItemsData.1
        }
        
        return (changes, updateModelClosure)
    }
    
    //MARK: - CollectionDataSourceDelegateProtocol
    func dataSourceDidUpdate(updateData: CollectionUpdateData,
                             updateType: CollectionUpdateType,
                             completion:  (() -> Void)?) {
        
        let newItems = updateData.items
        let newSections = updateData.sections
        state = newItems.isEmpty ? .empty : .feed
        processDataSourceUpdate(isEmpty: newItems.isEmpty)
        
        queue.addTask { [weak self] (taskCompletion) in
            if let oldItems = self?.itemCompanionCollection,
                let oldSections = self?.sections {
                self?.updateModels(newItemsData: (newItems, newSections),
                                   oldItemsData: (oldItems, oldSections),
                                   updateType: updateType,
                                   completion: {
                                    completion?()
                                    taskCompletion()
                })
            }
        }
    }
    
    //MARK: - Private functions
    private func presenterForIndexPath(_ indexPath: IndexPath) -> CollectionItemPresenterProtocol {
        let offset = sectionOffsets[safe: indexPath.section] ?? 0
        return self.presenterForIndex(indexPath.item + offset,
                                      feedItemCompanionCollection: self.itemCompanionCollection)
    }
    
    private func presenterForIndex(_ index: Int,
                                       feedItemCompanionCollection items: CompanionCollection) -> CollectionItemPresenterProtocol {
        return items[index].presenter
    }
    
    private func modelForIndexPath(
        _ index: Int,
        feedItemCompanionCollection items: CompanionCollection
    ) -> CollectionModelProtocol? {
        guard items.containsIndex(index) else { return nil }
        return items[index].item
    }
    
    /**
     Если нам требуется обновить видимые ячейки, то дергаем этот метод, который сделает
     всю работу без их перезагрузки
     
     - parameter changes: Объект с изменениями
     */
    fileprivate func updateVisibleCells(_ changes: CollectionChanges) {
        guard let visibleCells = visibleCells() else { return }
        
        let cellsToUpdate = CollectionChangesDetector.updated(collection: visibleCells,
                                                              withChanges: changes)
        cellsToUpdate.forEach { (indexPath, cell) in
            let presenter = self.presenterForIndexPath(indexPath)
            presenter.configureCell(cell,
                                    animated: false,
                                    additionalConfiguration: {})
            presenter.cellWillBeShown(cell)
        }
    }
    
    /**
     Получаем словарь с видимыми ячейками
     
     - returns: Словарь с соответствием ячеек и их Index path
     */
    fileprivate func visibleCells() -> [IndexPath: CellType]? {
        guard let indexPathsForVisibleRows = getModuleCollectionTypeView().indexPathsForVisibleCells() else { return nil }
        var visibleCells: [IndexPath: CellType] = [:]
        indexPathsForVisibleRows.forEach({ (indexPath) in
            if let cell = getModuleCollectionTypeView().cellForIndexPath(indexPath: indexPath) {
                visibleCells[indexPath as IndexPath] = cell
            }
        })
        return visibleCells
    }
    
    func getCell(uid: String) -> CellType? {
        guard let indexPath = getIndexPath(uid: uid) else { return nil }
        return visibleCells()?[indexPath]
    }
    
    func getIndexPath(uid: String) -> IndexPath? {
        guard let index = itemCompanionCollection.indexOf(uid) else { return nil }
        return CollectionChangesDetector.generateIndexPath(index: index, sections: sections.map { $0.items })
    }
    
    func getItem(for indexPath: IndexPath) -> CollectionModelProtocol? {
        guard let offset = sectionOffsets[safe: indexPath.section] else { return nil }
        return modelForIndexPath(indexPath.item + offset,
                                 feedItemCompanionCollection: self.itemCompanionCollection)
    }
    
    func getLastItem() -> CollectionModelProtocol? {
        return itemCompanionCollection.lastItem?.item
    }

    func moveItem(from initial: IndexPath, to destination: IndexPath) {
        guard let initialOffset = sectionOffsets[safe: initial.section],
            let destinationOffset = sectionOffsets[safe: destination.section] else {
            return
        }
        itemCompanionCollection.move(
            from: initial.item + initialOffset,
            to: destination.item + destinationOffset
        )
    }
}

//Тут мы обрабатываем данные
extension CollectionModulePresenter {
    
    func updateModels(newItemsData: ([ItemType], [SectionModelProtocol]),
                      oldItemsData: (CompanionCollection, [SectionModelProtocol]),
                      updateType: CollectionUpdateType,
                      completion: @escaping () -> Void) {
        
        let updateType = getModuleView().isFirstLayout ? .firstLoad : updateType
        let performInBackground = updateType != .firstLoad
        
        if performInBackground {
            DispatchQueue.global().async { [weak self] in
                guard let result = self?.createModelUpdates(
                    newItemsData: newItemsData,
                    oldItemsData: oldItemsData
                ) else { return }
                DispatchQueue.main.async { [weak self] in
                    let view = self?.getModuleView()
                    view?.performBatchUpdates(result.changes,
                                              updateType: updateType,
                                              beforeUpdateBlock: { [weak self] in
                                                result.updateModelClosure()
                                                self?.updateVisibleCells(result.changes)
                    })
                    completion()
                }
            }
        } else {
            let result = self.createModelUpdates(newItemsData: newItemsData,
                                                 oldItemsData: oldItemsData)
            let view = self.getModuleView()
            result.updateModelClosure()
            view?.reloadData()
            completion()
        }
    }
    
    func createCompanionCollection(items: [ItemType]) -> CompanionCollection {
        
        let companionItems = items.map { (item) -> CollectionItemCompanion in
            let presenter = self.createPresenterForItem(item)
            return CollectionItemCompanion(uid: item.uid, item: item, presenter: presenter)
        }
        
        return CompanionCollection(items: companionItems)
    }
    
    func createPresenterForItem(_ item: ItemType) -> CollectionItemPresenterProtocol {
        return self.presenterFactory.createItemPresenter(item)
    }
}
