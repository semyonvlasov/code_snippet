//
//  DBStorage+Promise.swift
//  togezzer-ios
//
//  Created by Семен Власов on 12.09.2018.
//  Copyright © 2018 tgz. All rights reserved.
//

import PromiseKit

extension DBStorage {
    func fetchBG<DTO, DBO>(_ builder: DTObjectBuilder<DTO, DBO>.Type,
                           filter: [NSPredicate] = [],
                           sort: [SortParams]? = nil,
                           limit: Int = 0,
                           returnInMainThread: Bool = true) -> Promise<[DTO]> {
        return Promise { promise in
            fetchBG(
                builder,
                filter: filter,
                sort: sort,
                limit: limit,
                returnInMainThread: returnInMainThread
            ) { dtos in
                promise.resolve(dtos, nil)
            }
        }
    }
}
