//
//  GroupMemberDTO.swift
//  togezzer-ios
//
//  Created by Семен Власов on 21/08/2019.
//  Copyright © 2019 tgz. All rights reserved.
//

import Foundation

struct GroupMemberDTO: DecoratedDTObjectProtocol {
    let baseDTO: DTObjectProtocol
    let userInfo: CompanyUserDTO
    let groupStatus: UserGroupStatus
    let role: GroupRole
    let groupId: Int
}

extension GroupMemberDTO {
    var user: UserDTO { return userInfo.userDTO }
}
