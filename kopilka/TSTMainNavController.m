//
//  TSTMainNavController.m
//  Kopilka
//
//  Created by Семен Власов on 01.07.14.
//  Copyright (c) 2014 SVV. All rights reserved.
//

#import "TSTMainNavController.h"
#import "TSTStringDefines.h"
#import "TSTRevealViewController.h"
#import "TSTCustomToolbar.h"
#import "TSTControllerObject.h"
#import "TSTUtils.h"

@interface TSTMainNavController () <UINavigationControllerDelegate>

@end

@implementation TSTMainNavController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recieveNotif:) name:notif_BackRowSelected object:nil];
    self.navigationBar.barTintColor = UIColorFromRGB(0xadd128);
    self.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    if (self.viewControllers && self.viewControllers.count > 0) {
        [self setLineNavigationButtonForVC:[self.viewControllers objectAtIndex:0]];
    }
}

- (void)recieveNotif:(NSNotification *)notif {
    if ([notif.name isEqualToString:notif_BackRowSelected]) {
        if (!notif.object) {
            [[TSTRevealViewController sharedInstance] showToggle];
        }
        else {
            TSTControllerObject *controllerObj = notif.object;
            if (controllerObj.controller != nil && controllerObj.action == ACTION_PUSH) {
                self.viewControllers = @[controllerObj.controller];
                [self setLineNavigationButtonForVC:controllerObj.controller];
                if ([TSTRevealViewController sharedInstance].curFrontViewPos == FrontViewPositionRight)
                    [[TSTRevealViewController sharedInstance] showToggle];
            }
            else if (controllerObj.action == ACTION_EXIT) {
                [[TSTRevealViewController sharedInstance].navigationController popToRootViewControllerAnimated:YES];
                [TSTUtils clearSavedAuthData];
            }
        }
    }
    else if ([notif.name isEqualToString:notif_Exit]) {
        [[TSTRevealViewController sharedInstance].navigationController popToRootViewControllerAnimated:YES];
        [TSTUtils clearSavedAuthData];
    }
}

- (void)setLineNavigationButtonForVC:(UIViewController *)viewController {
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setExclusiveTouch:YES];
    [leftButton addTarget:self action:@selector(lineButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"menuButt.png"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 5, 40, self.navigationBar.frame.size.height - 10);
    leftButton.clipsToBounds = NO;
    
    TSTCustomToolbar *lToolbar = [[TSTCustomToolbar alloc] initWithFrame:CGRectMake(0, 0, 80, self.navigationBar.frame.size.height + 40)];
    lToolbar.barStyle = -1;
    [lToolbar setBackgroundImage:[UIImage new]
              forToolbarPosition:UIToolbarPositionAny
                      barMetrics:UIBarMetricsDefault];
    
    [lToolbar setBackgroundColor:[UIColor clearColor]];
    
    UIBarButtonItem *spaceItem1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    spaceItem1.width = -24;
    
    UIBarButtonItem *spaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    spaceItem.width = 40;
    
    UIBarButtonItem *tmpLItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    tmpLItem.width = 40;
    [lToolbar setItems:[NSArray arrayWithObjects:spaceItem1, tmpLItem, spaceItem, nil]];
    
    viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:lToolbar];
}

- (void)lineButtonAction {
    [[TSTRevealViewController sharedInstance] showToggle];
}

- (void)navigationController:(UINavigationController *)navigationController
       didShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animate {
    self.interactivePopGestureRecognizer.enabled = NO;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
