//
//  BMPGLKViewController.m
//  BWRTSPPlayer
//
//  Created by adm on 07.04.16.
//  Copyright © 2016 Beward Ltd. All rights reserved.
//

#import "BMPGLKViewController.h"
#import "BMPFrameFormats.h"

typedef struct {
    CGPoint geometryVertex;
    CGPoint textureVertex;
} TexturedVertex;

typedef struct {
    TexturedVertex bl;
    TexturedVertex br;
    TexturedVertex tl;
    TexturedVertex tr;
} TexturedQuad;

@interface BMPGLKViewController () {
    GLuint _texture;
    BOOL _newFrame;
    BOOL _newFrameDrawed;
    BOOL _clearView;
}

@property (strong, nonatomic) BMPVideoFrame* frameToDraw;

@property (strong) GLKBaseEffect *effect;
@property (assign) TexturedQuad quad;

@end

@implementation BMPGLKViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Create an OpenGL ES context and assign it to the view loaded from storyboard
    GLKView *view = (GLKView *)self.view;
    view.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    // Configure renderbuffers created by the view
    view.drawableColorFormat = GLKViewDrawableColorFormatRGBA8888;
    view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
    view.drawableStencilFormat = GLKViewDrawableStencilFormat8;
    
    // Enable/Disable multisampling
//    view.drawableMultisample = GLKViewDrawableMultisample4X;
    view.drawableMultisample = GLKViewDrawableMultisampleNone;

    self.preferredFramesPerSecond = 60;
    
    self.effect = [[GLKBaseEffect alloc] init];
    
    //зона проекции. пусть будет вся вьюха!
    GLKMatrix4 projectionMatrix = GLKMatrix4MakeOrtho(0, self.view.bounds.size.width, 0, self.view.bounds.size.height, -512, 512);
    self.effect.transform.projectionMatrix = projectionMatrix;

    GLKMatrix4 modelMatrix = GLKMatrix4Identity;
    modelMatrix = GLKMatrix4Translate(modelMatrix, 0, self.view.bounds.size.height, 0);
    modelMatrix = GLKMatrix4Scale(modelMatrix, 1.0f, -1.0f, 0);
    self.effect.transform.modelviewMatrix = modelMatrix;
    
    _frameToDraw = nil;
    _newFrame = NO;
    _newFrameDrawed = NO;
    _clearView = YES;
    _texture = 0;

    TexturedQuad newQuad;
    newQuad.br.geometryVertex = CGPointMake(0, self.view.bounds.size.height);
    newQuad.bl.geometryVertex = CGPointMake(self.view.bounds.size.width, self.view.bounds.size.height);
    newQuad.tr.geometryVertex = CGPointMake(0, 0);
    newQuad.tl.geometryVertex = CGPointMake(self.view.bounds.size.width, 0);
    
    newQuad.br.textureVertex = CGPointMake(0, 1);
    newQuad.bl.textureVertex = CGPointMake(1, 1);
    newQuad.tr.textureVertex = CGPointMake(0, 0);
    newQuad.tl.textureVertex = CGPointMake(1, 0);
    
    self.quad = newQuad;
}

- (void)playRenderingLoop
{
    self.paused = NO;
    _clearView = NO;
}

- (void)pauseRenderingLoop
{
    self.paused = YES;
    _clearView = NO;

}

- (void)stopRenderingLoop
{
    self.paused = YES;
    _clearView = YES;

}

- (void)playRenderingLoop:(BOOL)play
{
    if(play) {
        self.paused = NO;
        _clearView = NO;
    } else {
        _clearView = YES;
    }
}

- (void)setFrame:(BMPVideoFrame*)frame
{
    if(self.paused) {
        return;
    }
    
    _frameToDraw = frame;
    _newFrame = YES;
    _newFrameDrawed = NO;
}

- (void)update
{
    if(_newFrame) {
        [self setTexture:_frameToDraw];
        _newFrame = NO;
    }
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    if(_clearView) {
        [self clearGLKView];
        self.paused = YES;
        return;
    }
    
    if(!_newFrameDrawed) {
        [self drawTexture];
        _newFrameDrawed = YES;
    }
}

- (void)clearGLKView
{
    glDeleteTextures(1, &_texture);
    _texture = 0;
    
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);
}

- (void)drawTexture
{
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    
    self.effect.texture2d0.name = _texture;
    self.effect.texture2d0.enabled = YES;
    
    [self.effect prepareToDraw];
    
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glEnableVertexAttribArray(GLKVertexAttribTexCoord0);
    
    long offset = (long)&_quad;
    
    glVertexAttribPointer(GLKVertexAttribPosition, 2, GL_FLOAT, GL_FALSE, sizeof(TexturedVertex), (void*) (offset + offsetof(TexturedVertex, geometryVertex)));
    glVertexAttribPointer(GLKVertexAttribTexCoord0, 2, GL_FLOAT, GL_FALSE, sizeof(TexturedVertex), (void*) (offset + offsetof(TexturedVertex, textureVertex)));
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    glDisableVertexAttribArray(GLKVertexAttribPosition);
    glDisableVertexAttribArray(GLKVertexAttribTexCoord0);
}

- (void)setTexture:(BMPVideoFrame*)frame
{
    BMPVideoFrameRGBA *rgbFrame = (BMPVideoFrameRGBA*)frame;
    
    if(!frame) {
        glDeleteTextures(1, &_texture);
        _texture = 0;
        return;
    }
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    
    if (_texture == 0) {
        glGenTextures(1, &_texture);
    }
    
    glBindTexture(GL_TEXTURE_2D, _texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (int)frame.width, (int)frame.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, rgbFrame.rgba.bytes);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

- (void)dealloc
{
    if (_texture) {
        glDeleteTextures(1, &_texture);
        _texture = 0;
    }
}

@end