//
//  BaseDeserializer.swift
//  togezzer-ios
//
//  Created by semyonvlasov on 16.10.2017.
//  Copyright © 2017 tgz. All rights reserved.
//

import Foundation

protocol Deserializer {
    associatedtype SerializableModel: RealmModel
    static func deserialize(data: [String: Any]) -> SerializableModel?
}
